import sys
import json

def help_me():
   print "USAGE"
   print "\t python "+sys.argv[0]+" <number_of_total_buses> "+" <output_folder> "
   print "\t Example:"
   print "\t\t python "+sys.argv[0]+" 30 "+" resource/input/12/bus_map/ "
   print "INFO"
   print "\tScript which generates input for bus_map based on the existing active input."
   print "\tIt needs to know the number of total buses in the city, this can be calculated from the configuration files originated for active."



def generate_config(busesNumber, output_dir):
  stopsPerBus      = busesNumber * 4
  stopsPerDistrict = busesNumber * 3
  if stopsPerDistrict > 10:
    stopsPerDistrict = 10
  if stopsPerBus > 10:
    stopsPerBus = 10
  config={
     "busesNumber" : busesNumber,
     "stopsPerBus" : stopsPerBus,
     "stopsPerDistrict" : stopsPerDistrict
  }
  print config
  return (config, output_dir)

def write_data(data):
   config   = data[0]
   out      = data[1]
   filename = "config_bus.json"
   with open(out + filename, "w+") as outfile:
      json.dump(config, outfile, indent=4)

if(len(sys.argv) != 3):
   print 'Invalid parameters'
   help_me()
   sys.exit()

write_data(generate_config(int(sys.argv[1]), sys.argv[2]))
