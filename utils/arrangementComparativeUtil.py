from utils.arrangementComparative import ArrangementComparative


class ArrangementComparativeUtil:
    @staticmethod
    def get_opposite(arrangement_comparator):
        if arrangement_comparator == ArrangementComparative.AFTER:
            opposite = ArrangementComparative.FIRST
        else:
            if ArrangementComparative.FIRST:
                opposite = ArrangementComparative.AFTER
        return opposite
