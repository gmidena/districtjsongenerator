import os
import sys


class FilePathInputReceiver:

    @staticmethod
    def receive(file_path):
        if os.path.exists(file_path):
            return file_path
        else:
            raise Exception('path ' + file_path + ' inesistente')