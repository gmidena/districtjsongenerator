from enum import Enum


class CardinalDirection(Enum):
    WEST = 0
    EAST = 1
    NORTH = 2
    SOUTH = 3
