from enum import Enum


class StraightDirection(Enum):
    SOUTH_NORTH = 0
    NORTH_SOUTH = 1
    WEST_EAST = 2
    EAST_WEST = 3
