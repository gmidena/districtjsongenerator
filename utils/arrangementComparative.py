from enum import Enum


class ArrangementComparative(Enum):
    FIRST = 0,
    AFTER = 1
