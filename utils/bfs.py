class BFS():

	def __init__(self):
		self.nodes = set()
		self.edges = {}

	def add_node(self, node, edges):
		self.nodes.add(node)
		self.edges[node] = edges

	def spanning_tree(self, initial):
		paths = {}
		visited = set()

		queue = [initial]
		paths[initial] = [ initial ]

		while queue:
			current = queue.pop(0)
			if current not in visited:
				visited.add(current)
				for reached in self.edges[current]:
					if reached not in visited and reached not in queue:
						paths[reached] = paths[current] + [ reached ]
						queue.append(reached)

		paths[initial].append(initial)
		return paths
