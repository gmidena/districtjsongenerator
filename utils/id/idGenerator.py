class IdGenerator:
    def __init__(self, starting_id = 0):
        self.id = starting_id

    def generate(self):
        self.id = self.id + 1
        return self.id
