from utils.idGenerator import IdGenerator


class GeneralIdGenerator:
    id = None

    @classmethod
    def init(cls, starting_id):
        cls.id = IdGenerator(starting_id)

    @classmethod
    def generate(cls):
        if cls.id is None:
            cls.id = IdGenerator()
        return cls.id.generate()
