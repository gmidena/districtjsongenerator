from utils.id.idGenerator import IdGenerator


class FacilityIdGenerator:
    id = None

    @classmethod
    def init(cls, starting_id):
        cls.id = IdGenerator(starting_id)

    @classmethod
    def generate(cls):
        if cls.id is None:
            cls.id = IdGenerator()
        return cls.id.generate()
