from utils.cardinalDirection import CardinalDirection
from utils.straightDirection import StraightDirection


class DirectionUtil:
    @staticmethod
    def get_source(straight_direction):
        if straight_direction == StraightDirection.EAST_WEST:
            source = CardinalDirection.EAST
        elif straight_direction == StraightDirection.WEST_EAST:
            source = CardinalDirection.WEST
        elif straight_direction == StraightDirection.NORTH_SOUTH:
            source = CardinalDirection.NORTH
        elif straight_direction == StraightDirection.SOUTH_NORTH:
            source = CardinalDirection.SOUTH
        return source

    @staticmethod
    def get_destination(straight_direction):
        if straight_direction == StraightDirection.EAST_WEST:
            destination = CardinalDirection.WEST
        elif straight_direction == StraightDirection.WEST_EAST:
            destination = CardinalDirection.EAST
        elif straight_direction == StraightDirection.NORTH_SOUTH:
            destination = CardinalDirection.SOUTH
        elif straight_direction == StraightDirection.SOUTH_NORTH:
            destination = CardinalDirection.NORTH
        return destination

    @staticmethod
    def get_left_direction(cardinal_direction):
        if cardinal_direction == CardinalDirection.EAST:
            left_direction = CardinalDirection.SOUTH
        elif cardinal_direction == CardinalDirection.SOUTH:
            left_direction = CardinalDirection.WEST
        elif cardinal_direction == CardinalDirection.WEST:
            left_direction = CardinalDirection.NORTH
        elif cardinal_direction == CardinalDirection.NORTH:
            left_direction = CardinalDirection.EAST
        return left_direction

    @staticmethod
    def get_right_direction(cardinal_direction):
        if cardinal_direction == CardinalDirection.EAST:
            right_direction = CardinalDirection.NORTH
        elif cardinal_direction == CardinalDirection.SOUTH:
            right_direction = CardinalDirection.EAST
        elif cardinal_direction == CardinalDirection.WEST:
            right_direction = CardinalDirection.SOUTH
        elif cardinal_direction == CardinalDirection.NORTH:
            right_direction = CardinalDirection.WEST
        return right_direction

    @staticmethod
    def get_opposite_direction(cardinal_direction):
        if cardinal_direction == CardinalDirection.EAST:
            opposite_direction = CardinalDirection.WEST
        elif cardinal_direction == CardinalDirection.SOUTH:
            opposite_direction = CardinalDirection.NORTH
        elif cardinal_direction == CardinalDirection.WEST:
            opposite_direction = CardinalDirection.EAST
        elif cardinal_direction == CardinalDirection.NORTH:
            opposite_direction = CardinalDirection.SOUTH
        return opposite_direction
