from random import randint

class Randomizer:
    def __init__(self):
        pass

    @staticmethod
    def select_random(arr):
        first_array_index = 0
        random_index = Randomizer.random(first_array_index, len(arr) - 1)
        return arr[random_index]

    @staticmethod
    def random(min, max):
        return randint(min, max)
