class FileWriter:
    def __init__(self):
        pass

    @staticmethod
    def write(fileName, data):
        out_file = open(fileName, "w")
        out_file.write(data)
        out_file.close()