import json


class JsonFileLoader:
    @staticmethod
    def load(file_directory):
        json_data = open(file_directory).read()
        return json.loads(json_data)