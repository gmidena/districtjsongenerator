#!/bin/bash


# Once there was these cities: 40, 144, 544, 2112, 8320
cities=( 12 12 ) # add cities here as well
lengths=( 7 9 ) # streets' length in cities

if [[ ${#cities[@]} != ${#lengths[@]} ]]; then
  echo "Different number of items provided for cities and lengths"
  exit 53
fi

unset_city_root=666

if [[ -z "$CITY_ROOT" ]]; then
  echo "Environment variable $CITY_ROOT is unset. Please fix this."
  exit $unset_city_root
fi

# generate city names
names=( )
i=0
for city in "${cities[@]}"
do
  names+=($i)
  i=$(( i+1 ))
done


input="resource/input/"
output="resource/output/"
collected="resource/collected/"
mw_config="resource/mw_config/"

# CREATE FOLDERS, IF NOT PRESENT
mkdir -p $output"ai/"
mkdir -p $output"broker/"
for name in "${names[@]}"
do
  mkdir -p $input
  mkdir -p $output
  mkdir -p $collected
  mkdir -p $mw_config
  mkdir -p $input$name
  mkdir -p $input$name"/"active/
  mkdir -p $input$name"/"bus_map/
  mkdir -p $input$name"/"districts/
  mkdir -p $output$name
  mkdir -p $output/interface_layer
  mkdir -p $output$name"/"active/
  mkdir -p $output$name"/"agendas/
  mkdir -p $output$name"/"districts/
  mkdir -p $collected$name
  mkdir -p $mw_config$name
  mkdir -p $mw_config$name/routing/
done

# DO THE CLEANING
for name in "${names[@]}"
do
  ./clean.sh $input$name"/" $output$name"/"
done

# INPUT GENERATION
j=0
for city in "${cities[@]}"
do
  len=${lengths[j]}
  name=${names[j]}
  ./generate_all_inputs.sh $name $city $len
  j=$(( j+1 ))
done

# CITY GENERATION
for name in "${names[@]}"
do
  python . $name $input$name"/" $output$name"/"
done

# COLLECTION OF DISTRICTS
for name in "${names[@]}"
do
  bash collector.sh district $name $output$name"/"districts/ $collected$name"/"
done

# INTERFACE LAYER
resources='{ "rx_pool_size" : 3, "activator_req_pool_size" : 2, "activator_ack_pool_size" : 2 }'
number_of_districts=$(ls $output$name"/"districts/ | wc -l)
for name in "${names[@]}"
do
  out_resources=$output"interface_layer/resources"$name".conf"
  out_hosts=$output"interface_layer/hosts"$name".conf"
  # generate resources
  echo $resources > $out_resources
  # generate hosts
  echo "{" > $out_hosts
  echo '"app_name": "backend'$name'",' >> $out_hosts
  echo '"app_port": 8082,' >> $out_hosts
  echo '"mw_name": "mw'$name'",' >> $out_hosts
  echo '"mw_port": 8081' >> $out_hosts
  echo "}" >> $out_hosts
done

# NAME RESOLUTION (FOR MIDDLEWARE)
collected_names=""
for name in "${names[@]}"
do
  collected_names=$name","$collected_names
  python config_for_mw.py $collected$name"/"district.json $mw_config$name"/" names.conf
done
# insert broker_to_as config file
collected_names="["$collected_names
collected_names=$(echo "${collected_names%?}]")
echo $collected_names > $output"broker/cities.conf"

# Routing tables
for name in "${names[@]}"
do
  python routing_tables.py $name $mw_config$name"/"routing/glob.conf $mw_config$name"/"routing/
done

# Automatize copy in $CITY_ROOT
for name in "${names[@]}"
do
  collected_info=$collected$name/district.json
  district_info=$output$name/
  ai_info=$output"ai/"
  interface_layer_info=$output"interface_layer/"
  mw_info=$mw_config$name
  ./copy_to_city_root.sh $name $district_info $mw_info $ai_info $interface_layer_info $collected_info $collected_names
done
