import json

from active.config.activeInfo import ActiveInfo

from utils.randomizer import Randomizer


class AgendaGenerator:

    @staticmethod
    def generate():
        actions = [ ]

        FIRST_ACTION_MAX_DELAY = 10000 # milliseconds

        pedestrians = ActiveInfo.get_pedestrians()
        for pedestrian in pedestrians:
            when = Randomizer.random(1, FIRST_ACTION_MAX_DELAY)
            actions.append( (pedestrian.get_id(), when) )

        buses = ActiveInfo.get_buses()
        for bus in buses:
            actions.append( (bus.get_id(), 1) )

        traffic_lights = ActiveInfo.get_traffic_lights()
        tl_period = ActiveInfo.get_traffic_light_period() * 1000
        for traffic_light in traffic_lights:
            traffic_light_id = traffic_light.get_id()
            if(traffic_light_id % 2 == 0):
                actions.append( (traffic_light_id, 1) )
            else:
                actions.append( (traffic_light_id, tl_period + 1) )

        return json.dumps({
            "actions" : AgendaGenerator.get_actions_json(actions),
            "elapsedTime" : 0
        }, indent=2, sort_keys=True)

    @staticmethod
    def get_actions_json(actions):
        actions_json = []
        for action in actions:
            actions_json.append( \
                json.loads( \
                    AgendaGenerator.get_json_for_action(action)))
        return actions_json

    @staticmethod
    def get_json_for_action(action):
        return json.dumps({
            "who" : action[0],
            "when" : action[1]
            })
