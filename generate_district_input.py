import json
import sys
import math
import copy
import pprint

from utils.randomizer import Randomizer
################################
# RULES FOR STREETS
# each set of streets forms a perfect square (grid)
# the number of vertical streets must be equal to
# the number of horizontal streets.
# Follow the numerical sequence of base generator for each perfect square (grid)
# to guarantee a complete connected grid:
#  base1 = base0*4 - 2^iteration
# starting with base0=3 and iteration=1
# RULES FOR INTERSECTIONS
# TOTAL NUMBER OF INTERSECTIONS
# NUMBER OF INTERSECTION OF GRADE = 2
#  Always 4, a square has 4 corners
# NUMBER OF INTERSECTION OF GRADE = 3
#  total_number_of_intersection1 - internal_square =
#  total_number_of_intersection1 - total_number_of_iintersections0
# NUMBER OF INTERSECTION OF GRADE = 4
#  total_number_of_intersection1 - grade3
# ------------------------------------------------------------------------------
# NUMBER OF ROWS = NUMBER OF COLUMNS
#  square_root(grade4)
#######################################

def help_me():
  sizes=(check_base(0,20))[3]
  print "USAGE"
  print "\t python " + sys.argv[0] + " <number_of_streets> " + " <max_number_of_stretches_for_a_street> " + " <output_folder> <mw_config_routing>"
  print "\t Example:"
  print "\t\t python " + sys.argv[0] + " 40 " + " 30 " + " resource/input/districts/40/ resource/mw_config/40/routing/"
  print "INFO"
  print "\tScript which generates input for district. Since it generates only perfect grids as composition of squares of the same dimension, it is not flexible in the number of possible sizes:"
  print "\tAllowed sizes (number of streets of the entire city): "
  print sizes
  print "\t You can specify the maximum range of stretches, which is the max number of stretches that might be in a street. They will be generated randomly."
  print "\tThe script splits the city in several districts, the minimum number of districts is 4. Bigger is the city and more districts it will generate (also bigger districts)"
  print "PS:"
  print "\t This code sucks, but it works like a charm."

def check_base(number_of_streets, number_of_tries):
  base0    = 2
  base_res = 0
  it_res   = 0
  sizes    = []
  for i in range(0,number_of_tries):
    base1 = 3
    sizes.append(base1 * 4)
    if(number_of_streets == base1*4):
      base_res = base1
      it_res = i
      return (True, base_res, it_res)
  return (False, base_res, it_res, sizes)

def make_street():
  return { "id": 0, "length": 0, "orientation": ""}

def make_exit():
  return { "streetId": 0, "direction": ""}

def get_new_directions(intersection, checked, new_directions):
  missing = ""
  crossed_direction = \
    {"SOUTH" : ["WEST", "EAST"], "WEST" : ["NORTH", "SOUTH"], \
     "NORTH" : ["WEST", "EAST"], "EAST" : ["NORTH", "SOUTH"]}
  fix_direction = \
    {"SOUTH" : "NORTH", "WEST" : "EAST", "NORTH" : "SOUTH", "EAST" : "WEST"}
  for key, value in checked.iteritems():
    if(not value):
      missing = key
      break;
  # new_direction = opposite_direction[missing]
  for exit in intersection:
    if exit["direction"] in crossed_direction[missing]:
      # we do not need a list, each corner has only 1 new direction
      insert = fix_direction[missing]
      new_directions[exit["streetId"]] = insert
  return new_directions

def get_borders(intersections):
  borders = []
  new_directions = {}
  for intersection in intersections:
    # detect borders
    if(len(intersection) == 3):
      borders.append(intersection)
      checked = \
        { "NORTH" : False, "SOUTH" : False, "WEST" : False, "EAST" : False }
      for exit in intersection:
        checked[exit["direction"]] = True
      new_directions = \
        get_new_directions(intersection, checked, new_directions)
  return (borders, intersections, new_directions)

def get_corners(data):
  borders            = data[0]
  intersections      = data[1]
  new_directions     = data[2]
  streets_directions = {}
  # record the intersection direction for each street
  for exits in borders:
    for exit in exits:
      # if it is undefined
      if(not (exit["streetId"] in streets_directions)):
        streets_directions[exit["streetId"]] = []
      streets_directions[exit["streetId"]].append(exit["direction"])
  # remove intersections which are not in the corners (4 way intersections)
  for intersection in intersections:
    if(len(intersection) == 4):
      for key in streets_directions.keys():
        for exit in intersection:
          if key == exit["streetId"]:
            del streets_directions[key]
  # reverse directions - the correct new directions are reversed
  opposite_direction = \
    {"SOUTH" : "NORTH", "WEST" : "EAST", "NORTH" : "SOUTH", "EAST" : "WEST"}
  for k,v in streets_directions.iteritems():
    streets_directions[k] = opposite_direction[v[0]]
  return (streets_directions, new_directions)

def make_grade2_intersections(intersections):
  (streets_directions, new_directions) = \
    get_corners(get_borders(intersections))
  grade2 = []
  size = len(streets_directions)
  size = size / 2
  index = 0
  top_left = 0
  for k0, v0 in streets_directions.iteritems():
    if index == size:
      break;
    for k1, v1 in streets_directions.iteritems():
      # found an intersection of grade 2
      if new_directions[k0] == v1 and new_directions[k1] == v0:
        grade2.append([
          { "streetId" : k0, "direction" : v0},
          { "streetId" : k1, "direction" : v1}])
        break;
    if new_directions[k0] == "SOUTH" and new_directions[k1] == "EAST" or \
       new_directions[k1] == "SOUTH" and new_directions[k0] == "EAST" :
      top_left = index
    index = index + 1
  return (grade2, top_left)



def make_grade3_intersections(streets, index, edges4):
  intersections = []
  prev_index = index
  for node in edges4["NORTH"]:
    # exits is a grade3 intersection
    exits = []
    exitN = node
    exitE = make_exit()
    exitE["streetId"] = prev_index
    prev_index = index
    index = index + 1
    exitE["direction"] = "EAST"
    exitW = make_exit()
    exitW["streetId"]  = index
    exitW["direction"] = "WEST"
    exits.append(exitE)
    exits.append(exitN)
    exits.append(exitW)
    item = {}
    item["exits"] = exits
    intersections.append(exits)
    index = index + 1
  prev_index = 0
  for node in edges4["SOUTH"]:
    # exits is a grade3 intersection
    exits = []
    exitS = node
    exitE = make_exit()
    exitW = make_exit()
    exitE["streetId"] = index
    if(not (prev_index == 0)):
      exitE["streetId"] = prev_index
    else:
      index = index + 1
    exitE["direction"] = "EAST"
    exitW["streetId"]  = index
    exitW["direction"] = "WEST"
    prev_index = index
    exits.append(exitE)
    exits.append(exitS)
    exits.append(exitW)
    item = {}
    item["exits"] = exits
    intersections.append(exits)
    index = index + 1
  prev_index = 0
  for node in edges4["EAST"]:
    # exits is a grade3 intersection
    exits = []
    exitS = make_exit()
    exitE = node
    exitN = make_exit()
    exitS["streetId"] = index
    if(not (prev_index == 0)):
      exitS["streetId"] = prev_index
    else:
      index = index + 1
    exitS["direction"] = "SOUTH"
    exitN["streetId"]  = index
    exitN["direction"] ="NORTH"
    prev_index=index
    exits.append(exitS)
    exits.append(exitE)
    exits.append(exitN)
    item = {}
    item["exits"] = exits
    intersections.append(exits)
    index = index + 1
  prev_index = 0
  for node in edges4["WEST"]:
    # exits is a grade3 intersection
    exits = []
    exitN = make_exit()
    exitW = node
    exitS = make_exit()
    exitN["streetId"] = index
    if(not (prev_index == 0)):
      exitX["streetId"] = prev_index
    else:
      index = index + 1
    exitN["direction"] = "NORTH"
    exitS["streetId"]  =  index
    exitS["direction"] = "SOUTH"
    prev_index = index
    exits.append(exitN)
    exits.append(exitW)
    exits.append(exitS)
    item = {}
    item["exits"] = exits
    intersections.append(exits)
    index = index + 1
  return intersections

def make_grade4(streets, index, prev_east, prev_south):
  direction  = ["SOUTH", "WEST", "NORTH", "EAST"]
  # exits is a grade4 intersection
  exits      = []
  next_index = 0
  sindex     = index
  for i in range(index, index + 4):
    exit = make_exit()
    exit["streetId"] = sindex
    if(bool(prev_south) or bool(prev_east)):
      if(bool(prev_south) and direction[i % 4] == "SOUTH"):
        exit["streetId"] = prev_south
        sindex = sindex - 1
      if(bool(prev_east) and direction[i % 4] == "EAST"):
        exit["streetId"] = prev_east
        sindex = sindex - 1
    exit["direction"] = direction[i % 4]
    exits.append(exit)
    sindex = sindex + 1
  if(next_index == 0):
    next_index = sindex
  return (exits, next_index)

def generate_streets(number_of_streets, street_length):
  streets = []
  # example: orientation[0] == VERTICAL
  orientation = ["VERTICAL", "HORIZONTAL"]
  if(street_length < 5):
    print 'Max street length must be at least 5'
    help_me()
    sys.exit()
  result=check_base(int(number_of_streets), 20)
  if(not result[0]):
    print 'The number of streets must enable to create a perfect grid available grid sizes : '
    print result[3]
    help_me()
    sys.exit()
  for i in range(0,int(number_of_streets)):
    street = make_street()
    street["id"] = i
    street["length"] = int(street_length)
    street["orientation"] = orientation[(i%2)]
    streets.append(street)
  return (streets, result)

def generate_exits():
  exits = []

def generate_intersections(input):
  # street is sorted by id (default sorting)
  streets       = input[0]
  base          = input[1][1]
  iterations    = input[1][2]
  intersections = []
  row           = base / (2**iterations)
  # total intersections
  total         = row*row
  # compute grade 2
  grade2        = 4
  # subtract the 4 corners (not real intersections)
  total         = total - grade2
  # compute grade3 (exclude corners)
  grade3        = (row - 2) * 4
  # compute grade4
  grade4        = total - grade3
  # generate all the intersection
  # (note: we only have grade3 and grade4 intersections)
  # generate all the grade4 intersection
  street_index  = 0
  sqr4          = int(math.sqrt(grade4))
  edges4        = { "SOUTH" : [], "WEST" : [], "NORTH" : [], "EAST" : []}
  direction     = ["SOUTH", "WEST", "NORTH", "EAST"]
  # matrix rows*cols
  grade4matrix  = [[0 for y in range(sqr4)] for x in range(sqr4)]
  for row in range(0,len(grade4matrix)):
    prev_south  = {}
    prev_east   = {}
    for colmn in range(0, len(grade4matrix[0])):
      # get north node id which will be the next south
      found = False
      if(not grade4matrix[row-1][colmn] == 0):
        for node in (grade4matrix[row-1][colmn]):
          if(node["direction"] == "NORTH"):
            prev_south = node["streetId"]
            found = True
          if(found):
            break
      if(grade4matrix[row][colmn] == 0):
        (grade4matrix[row][colmn], street_index) = \
          make_grade4(streets, streets[street_index]["id"],
                      prev_east, prev_south)
      # get west node id which will be the next east
      found=False
      for node in (grade4matrix[row][colmn]):
        if(node["direction"] == "WEST"):
          prev_east = node["streetId"]
          found = True
        if(found):
          break
      # collect first row streetsIds (SOUTH)
      if(row == 0):
        for node in grade4matrix[row][colmn]:
          if(node["direction"] == "SOUTH"):
            nodecp=copy.deepcopy(node)
            nodecp["direction"] = "NORTH"
            edges4["NORTH"].append(nodecp)
      # collect last row streetsIds (NORTH)
      if(row == len(grade4matrix) - 1):
        for node in grade4matrix[row][colmn]:
          if(node["direction"] == "NORTH"):
            nodecp=copy.deepcopy(node)
            nodecp["direction"] = "SOUTH"
            edges4["SOUTH"].append(nodecp)
    ## necessary to complete the grid and connect with grade3
    # collect first column west (WEST)
    for node in grade4matrix[row][0]:
      if(node["direction"] == "EAST"):
        nodecp=copy.deepcopy(node)
        nodecp["direction"] = "WEST"
        edges4["WEST"].append(nodecp)
    # collect last column east (EAST)
    for node in grade4matrix[row][len(grade4matrix[0]) - 1]:
      if(node["direction"] == "WEST"):
        nodecp=copy.deepcopy(node)
        nodecp["direction"] = "EAST"
        edges4["EAST"].append(nodecp)
  # compute grade3 intersections
  intersections = \
    make_grade3_intersections(streets, streets[street_index]["id"], edges4)
  # fill intersections with missing grade4 interesections
  for row in range(0, len(grade4matrix)):
    for colmn in range(0, len(grade4matrix[0])):
      intersections.append(grade4matrix[row][colmn])
  # fix orientations
  for exits in intersections:
    for node in exits:
      # node.id == node position in array
      node_ref = streets[node["streetId"]]
      if(node["direction"] == "EAST" or node["direction"] == "WEST"):
        node_ref["orientation"] = "HORIZONTAL"
      else:
        node_ref["orientation"] = "VERTICAL"
  # add the missing grade2 intersections, 1 for each corner
  (grade2, top_left) = make_grade2_intersections(intersections);
  intersections = intersections + grade2
  return (streets, intersections, grade2[top_left])




def has_next_exit(exits, direction):
  for exit in exits:
    if(exit["direction"] == direction):
      return True
  return False

# look for the next intersection in *all* the city intersections
def get_next_exits(intersections, exits, direction):
  opposite_direction = \
    {"SOUTH" : "NORTH", "WEST" : "EAST", "NORTH" : "SOUTH", "EAST" : "WEST"}
  to_found = { "direction" : None, "streetId" : None }
  for exit in exits:
    if(exit["direction"] == direction):
      to_found              = make_exit()
      to_found["streetId"]  = exit["streetId"]
      to_found["direction"] = opposite_direction[direction]
      break
  for exits in intersections:
    for exit in exits:
      if(exit["direction"] == to_found["direction"] and \
         exit["streetId"]  == to_found["streetId"]):
        return exits
  return []

def remove_duplicated_streets(districts):
  # get rid of duplicates inside the same district
  for district in districts:
    streets = []
    for street in district["streets"]:
      if street["id"] not in map(lambda x: x["id"], streets):
        streets.append(street)
    district["streets"] = streets
  # get rid of duplicates across different districts
  for i in range(1, len(districts)):
    for j in range(0,i):
      districts[i]["streets"] = \
        [x for x in districts[i]["streets"] \
            if x not in districts[j]["streets"]]
  return districts

def create_district(streets, exits):
  if exits == []:
    return { "streets" : streets, "intersections" : [] }
  else:
    return { "streets" : streets, "intersections" : [{ "exits" : exits }] }

def add_street(district, street):
  district["streets"].append(street)
  return district

def add_intersection(district, exits):
  district["intersections"].append({ "exits" : exits })
  return district

def new_district(districts):
  districts.append(create_district([], []))
  return districts[-1]

def generate_rows(streets, intersections, top_left):
  # top left corner of the grid
  exits         = top_left
  # create district with only the top left corner
  rows          = [create_district([], exits)]
  district      = rows[0]
  # insert the related streets
  for exit in exits:
    exit_street = streets[exit["streetId"]]
    if not exit_street in district["streets"]:
      add_street(district, streets[exit["streetId"]])
  exits = get_next_exits(intersections, exits, "EAST")
  # process the top edge of the grid
  while(has_next_exit(exits, "EAST")):
    for exit in exits:
      exit_street = streets[exit["streetId"]]
      if not exit_street in district["streets"]:
        add_street(district, exit_street)
      if not { "exits" : exits } in district["intersections"]:
        add_intersection(district, exits)
    if(has_next_exit(exits, "EAST")):
      exits = get_next_exits(intersections, exits, "EAST")
  # insert the top right corner (which has no next)
  add_intersection(district, exits)
  # insert the related streets
  for exit in exits:
    exit_street = streets[exit["streetId"]]
    if(not exit_street in district["streets"]):
      add_street(district, streets[exit["streetId"]])
  district = new_district(rows)
  # get the next row of the grid from the top left corner
  exits = get_next_exits(intersections, top_left, "SOUTH")
  while(has_next_exit(exits, "EAST")):
    first = exits
    # iterate the columns
    while(has_next_exit(exits, "EAST")):
      for exit in exits:
        exit_street = streets[exit["streetId"]]
        if(not exit_street in district["streets"]):
          add_street(district, streets[exit["streetId"]])
      add_intersection(district, exits)
      exits = get_next_exits(intersections, exits, "EAST")
    # add the last column (grade3)
    for exit in exits:
      exit_street = streets[exit["streetId"]]
      if(not exit_street in district["streets"]):
        add_street(district, streets[exit["streetId"]])
    add_intersection(district, exits)
    # the first of the next row
    exits = get_next_exits(intersections, first, "SOUTH")
    district = new_district(rows)
  return rows[:-1]

def generate_columns(row, cut_size, streets):
  columns = []
  MIN_CUT_SIZE = 2  # constant
  district = new_district(columns)
  row_intersections = copy.deepcopy(row["intersections"])
  intersections_no = len(row_intersections)
  fixed_inters      = map(lambda x: x["exits"], row_intersections)
  first_intersection = \
    filter(
      lambda x: "WEST" not in [exit["direction"] for exit in x["exits"]],
      row_intersections
    )[0]
  first_exits = first_intersection["exits"]
  # initially, next exits is the first intersection
  next_exits = first_exits
  while next_exits != []:
    if len(district["intersections"]) == cut_size \
       and intersections_no >= MIN_CUT_SIZE:
      district = new_district(columns)
    for exit in next_exits:
      exit_street = streets[exit["streetId"]]
      add_street(district, exit_street)
    add_intersection(district, next_exits)
    next_exits = get_next_exits(fixed_inters, next_exits, "EAST")
    intersections_no = intersections_no - 1
  return columns

# TODO: Clean this procedure and output path?
def generate_routing_tables(city):
  routing_infos = []
  for row in range(0, len(city)):
    len_row  = len(city[row])
    nodes_no = len_row * len(city)
    cols     = range(0, len(city[row]))

    for col in cols:
      routing_info = {}
      node_id = row * len_row + col
      left  = row * len_row + (node_id + nodes_no - 1) % len_row
      right = row * len_row + (node_id + nodes_no + 1) % len_row
      up    = (node_id - len_row) % nodes_no
      down  = (node_id + len_row) % nodes_no
      city_name = str(sys.argv[5])
      routing_info["id"] = row * len_row + col
      routing_info["id"] = city_name + "_" + str(routing_info["id"] + 1)
      routing_info["neighbors"] = \
        map(lambda x: city_name + "_" + str(x + 1), [left, right, up, down])
      routing_infos.append(routing_info)
      # generate routing table for

  out_path=str(sys.argv[4])
  with open(out_path + "/glob.conf", "w+") as outfile:
    json.dump(routing_infos, outfile, indent=2)

def split_city_in_districts(data):
  streets       = data[0]
  intersections = data[1]
  top_left      = data[2]
  # IMPORTANT: CUT SIZE
  cut_size      = 3
  rows          = generate_rows(streets, intersections, top_left)
  city          = []
  for row in rows:
    city.append(generate_columns(row, cut_size, streets))
  # generate routing tables
  generate_routing_tables(city)
  # then print district input
  districts = []
  for row in city:
    districts.extend(row)
  # remove duplicates
  districts = remove_duplicated_streets(districts)
  return districts

def write_data(districts):
  out_path=str(sys.argv[3])
  for i in range(1, len(districts)+1):
    with open(out_path+"in-"+sys.argv[5]+"_"+str(i)+".json", "w+") as outfile:
      json.dump(districts[i-1], outfile, indent=4)

if(len(sys.argv) != 6):
  print 'Invalid parameters'
  help_me()
  sys.exit()

write_data( \
  split_city_in_districts( \
    generate_intersections( \
      generate_streets(sys.argv[1], sys.argv[2]))))
