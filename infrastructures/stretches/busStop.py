import json


class BusStop:

    def __init__(self, id):
        self.id = id
        self.waiting_list = []
        self.stops = []

    def get_json(self):
        return json.dumps({
            "id": self.id,
            "waitingList": self.waiting_list,
            "stops": self.stops
        })

    def add_stop_list(self, stops_list):
        self.stops.extend(stops_list)
