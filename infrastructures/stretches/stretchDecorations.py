import json


class StretchDecorations:

    def __init__(self, pedestrian_crossing=False, bicycle_crossing=False, facility_id=None):
        self.pedestrian_crossing = pedestrian_crossing
        self.bicycle_crossing = bicycle_crossing
        self.facility_id = facility_id
        self.bus_stop_list = []

    def get_json(self):
        return json.dumps({
            "pedestrianCrossing": self.pedestrian_crossing,
            "bicycleCrossing": self.bicycle_crossing,
            "busStop": self.get_bus_stops_json(),
            "facilityId": self.facility_id
        })

    def add_bus_stop(self, bus_stop):
        self.bus_stop_list.append(bus_stop)

    def has_bus_stop(self):
        return self.bus_stop_list

    def get_bus_stops_json(self):
        if self.bus_stop_list == []:
            return None
        bus_stops_json = []
        for bus_stop in self.bus_stop_list:
            bus_stops_json.append(json.loads(bus_stop.get_json()))
        return bus_stops_json
