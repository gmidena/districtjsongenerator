import json

from infrastructures.stretches.busStop import BusStop
from infrastructures.stretches.stretchDecorations import StretchDecorations


class Stretch:

    def __init__(self, id, size, pedestrian_crossing=False, bicycle_crossing=False, facility_id=None):
        self.id = id
        self.size = size
        self.travellers = []
        self.decorations = StretchDecorations(pedestrian_crossing,
                                              bicycle_crossing,
                                              facility_id)

    def get_json(self):
        return json.dumps({
            "id": self.id,
            "size": self.size,
            "travellers": self.travellers,
            "decorations": json.loads(self.decorations.get_json())
        })

    def add_bus(self, bus):
        if len(self.travellers) > 0:
            return None
        self.travellers.append(bus)
        return { "roadway_stretch" : self }

    def add_bus_stop(self, bus, bus_track):
        bus_stop = BusStop(bus)
        bus_stop.add_stop_list(bus_track)
        self.decorations.add_bus_stop(bus_stop)

    def has_bus_stop(self):
        return self.decorations.has_bus_stop()

    def change_id(self, new_id):
        self.id = new_id

    def get_id(self):
        return self.id

    def set_facility_id(self, facility_id):
        self.decorations.facility_id = facility_id

    def set_pedestrian_crossing(self, pedestrian_crossing_presence):
        self.decorations.pedestrian_crossing = pedestrian_crossing_presence;

    def set_bicycle_crossing(self, bicycle_crossing_presence):
        self.decorations.bicycle_crossing = bicycle_crossing_presence;
