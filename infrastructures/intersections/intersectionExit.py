import json


class IntersectionExit:

    def __init__(self, street_id, direction, traffic_light_id):
        self.street_id = street_id
        self.stretches = []
        self.direction = direction
        self.traffic_light_id = traffic_light_id
        self.travellers = []

    def get_json(self):
        return json.dumps({
            "streetId": self.street_id,
            "stretchId": self.stretches,
            "direction": self.direction.name,
            "trafficLightId": self.traffic_light_id,
            "travellers": self.travellers
        })

    def add_traveller(self, traveller):
        self.travellers.append(traveller)

    def add_stretch(self, way, lane, stretch):
        self.stretches.append( (way.id, lane.id, stretch.id) )
