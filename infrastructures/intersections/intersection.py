import json

from infrastructures.intersections.intersectionExit import IntersectionExit
from utils.directionUtil import DirectionUtil


class Intersection:

    def __init__(self, id):
        self.id = id
        self.exits = {}

    def connect_street(self, street, exit_direction, traffic_light_id):
        exit = IntersectionExit( \
                    street.id, exit_direction, traffic_light_id)
        roadways = street.roadways
        for way in roadways:
            for lane in way.lanes:
                if DirectionUtil.get_source(lane.direction) == exit_direction:
                    exit.add_stretch(way, lane, lane.stretches[0])
        bikeways = street.bikeways
        for way in bikeways:
            for lane in way.lanes:
                if DirectionUtil.get_source(lane.direction) == exit_direction:
                    exit.add_stretch(way, lane, lane.stretches[0])
        footways = street.footways
        for way in footways:
            for lane in way.lanes:
                if DirectionUtil.get_source(lane.direction) == exit_direction:
                    exit.add_stretch(way, lane, lane.stretches[0])
        self.exits[exit_direction] = exit

    def get_json(self):
        return json.dumps({
            "id": self.id,
            "exits": self.get_exits_json()
        })

    def get_exits_json(self):
        exits_json = []

        for exit in self.exits.values():
            exits_json.append(json.loads(exit.get_json()))

        return exits_json
