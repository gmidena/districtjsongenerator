from infrastructures.intersections.intersection import Intersection
from utils.id.treadableIdGenerator import TreadableIdGenerator


class IntersectionGenerator:
    def __init__(self):
        pass

    @staticmethod
    def generate():
        intersection_id = TreadableIdGenerator.generate()
        return Intersection(intersection_id)
