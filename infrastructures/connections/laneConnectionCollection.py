import json


class LaneConnectionCollection:

    def __init__(self, source_lane_id):
        self.source_lane_id = source_lane_id
        self.destinations = []

    def add_destination(self, street_id, lane_id, way_id):
        self.destinations.append([street_id, lane_id, way_id])

    def add_destinations(self, destination_lanes):
        self.destinations.extend(destination_lanes)

    def get_json(self):
        return json.dumps({
            "out": self.source_lane_id,
            "ins": self.destinations
        })
