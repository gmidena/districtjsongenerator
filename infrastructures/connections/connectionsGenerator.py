from infrastructures.connections.laneConnectionCollection import LaneConnectionCollection
from infrastructures.connections.streetConnectionCollection import StreetConnectionCollection
from utils.cardinalDirection import CardinalDirection
from utils.directionUtil import DirectionUtil
from utils.arrangementComparative import ArrangementComparative


class ConnectionsGenerator:
  @staticmethod
  def generate(intersection, streets):
    connections = []

    # for each intersection exit
    for entrance_direction in intersection.exits:
      source_street_id = intersection.exits[entrance_direction].street_id
      source_street = streets[source_street_id]
      street_connection = StreetConnectionCollection(source_street_id)

      # for all other exits of the same intersection
      for exit_direction in intersection.exits:
        destination_street_id = intersection.exits[exit_direction].street_id
        if destination_street_id != source_street_id:
          destination_street = streets[destination_street_id]
          roadway_lane_connections = ConnectionsGenerator\
            .generate_lane_connections(entrance_direction, exit_direction,
                           source_street.roadways,
                           destination_street_id,
                           destination_street.roadways)
          street_connection.add_connections(roadway_lane_connections)

      # generate connections with lanes to the left (if an exit for the left side exists)
      footway_left_connections = bikeway_left_connections = []
      left_direction = DirectionUtil.get_left_direction(entrance_direction)
      if left_direction in intersection.exits:
        left_exit = intersection.exits[left_direction]
        destination_street = streets[left_exit.street_id]

        footway_left_connections = ConnectionsGenerator\
          .generate_left_connections(entrance_direction, left_direction,
                         destination_street, destination_street.footways,
                         source_street, source_street.footways)
        if len(footway_left_connections) > 0:
          street_connection.add_connections(footway_left_connections)

        bikeway_left_connections = ConnectionsGenerator \
          .generate_left_connections(entrance_direction, left_direction,
                         destination_street, destination_street.bikeways,
                         source_street, source_street.bikeways)
        if len(bikeway_left_connections) > 0:
          street_connection.add_connections(bikeway_left_connections)

      # otherwise generate connections with lanes to the front
      if not footway_left_connections or not bikeway_left_connections:
        opposite_direction = DirectionUtil.get_opposite_direction(entrance_direction)
        opposite_exit = intersection.exits[opposite_direction]
        destination_street = streets[opposite_exit.street_id]

        if not footway_left_connections:
          footway_opposite_connections = ConnectionsGenerator \
            .generate_straight_connections_to_the_left(entrance_direction, opposite_direction,
                                   destination_street, destination_street.footways,
                                   source_street, source_street.footways)
          street_connection.add_connections(footway_opposite_connections)

        if not bikeway_left_connections:
          bikeway_opposite_connections = ConnectionsGenerator \
            .generate_straight_connections_to_the_left(entrance_direction, opposite_direction,
                                   destination_street, destination_street.bikeways,
                                   source_street, source_street.bikeways)
          street_connection.add_connections(bikeway_opposite_connections)

      # generate connections with lanes to the right (if an exit for the right side exists)
      footway_right_connections = bikeway_right_connections = []
      right_direction = DirectionUtil.get_right_direction(entrance_direction)
      if right_direction in intersection.exits:
        right_exit = intersection.exits[right_direction]
        destination_street = streets[right_exit.street_id]

        footway_right_connections = ConnectionsGenerator \
          .generate_right_connections(entrance_direction, right_direction,
                        destination_street, destination_street.footways,
                        source_street, source_street.footways)
        if len(footway_right_connections) > 0:
          street_connection.add_connections(footway_right_connections)

        bikeway_right_connections = ConnectionsGenerator \
          .generate_right_connections(entrance_direction, right_direction,
                        destination_street, destination_street.bikeways,
                        source_street, source_street.bikeways)
        if len(bikeway_right_connections) > 0:
          street_connection.add_connections(bikeway_right_connections)

      # otherwise generate connections with lanes to the front
      if not footway_right_connections or not bikeway_right_connections:
        opposite_direction = DirectionUtil.get_opposite_direction(entrance_direction)
        opposite_exit = intersection.exits[opposite_direction]
        destination_street = streets[opposite_exit.street_id]

        if not footway_right_connections:
          footway_opposite_connections = ConnectionsGenerator \
            .generate_straight_connections_to_the_right(entrance_direction, opposite_direction,
                                  destination_street, destination_street.footways,
                                  source_street, source_street.footways)
          street_connection.add_connections(footway_opposite_connections)

        if not bikeway_right_connections:
          bikeway_opposite_connections = ConnectionsGenerator \
            .generate_straight_connections_to_the_right(entrance_direction, opposite_direction,
                                  destination_street, destination_street.bikeways,
                                  source_street, source_street.bikeways)
          street_connection.add_connections(bikeway_opposite_connections)

      connections.append(street_connection)
    return connections

  @staticmethod
  def generate_lane_connections(entrance_direction, exit_direction,
                  source_ways, destination_street_id, destination_ways):
    lane_connections = []
    for source_way in source_ways:  # for each source way
      for source_lane in source_way.lanes:  # for each lane into the source way
        if DirectionUtil.get_destination(source_lane.direction) != entrance_direction:
          lane_connection = LaneConnectionCollection(source_lane.id)
          for destination_way in destination_ways:  # for each destination way
            for destination_lane in destination_way.lanes:  # for each lane into the destination way
              if DirectionUtil.get_source(destination_lane.direction) != exit_direction:
                lane_connection.add_destination(destination_street_id, destination_lane.id, destination_way.id)
          lane_connections.append(lane_connection)
    return lane_connections

  @staticmethod
  def generate_left_connections(entrance_direction, exit_direction,
                  destination_street, destination_ways,
                  source_street, source_ways):
    if entrance_direction == CardinalDirection.WEST or entrance_direction == CardinalDirection.NORTH:
      destination_arrangement_to_roadway = ArrangementComparative.FIRST
    else:
      destination_arrangement_to_roadway = ArrangementComparative.AFTER

    if entrance_direction == CardinalDirection.WEST or entrance_direction == CardinalDirection.SOUTH:
      source_arrangement_to_roadway = ArrangementComparative.FIRST
    else:
      source_arrangement_to_roadway = ArrangementComparative.AFTER

    return ConnectionsGenerator \
      .generate_connections_for_lanes_of_ways_arranged_to_roadway(entrance_direction, exit_direction,
                                    destination_arrangement_to_roadway,
                                    source_arrangement_to_roadway,
                                    destination_street, destination_ways,
                                    source_street, source_ways)

  @staticmethod
  def generate_right_connections(entrance_direction, exit_direction,
                   destination_street, destination_ways,
                   source_street, source_ways):
    if entrance_direction == CardinalDirection.WEST or entrance_direction == CardinalDirection.NORTH:
      destination_arrangement_to_roadway = ArrangementComparative.FIRST
    else:
      destination_arrangement_to_roadway = ArrangementComparative.AFTER

    if entrance_direction == CardinalDirection.EAST or entrance_direction == CardinalDirection.NORTH:
      source_arrangement_to_roadway = ArrangementComparative.FIRST
    else:
      source_arrangement_to_roadway = ArrangementComparative.AFTER

    return ConnectionsGenerator \
      .generate_connections_for_lanes_of_ways_arranged_to_roadway(entrance_direction, exit_direction,
                                    destination_arrangement_to_roadway,
                                    source_arrangement_to_roadway,
                                    destination_street, destination_ways,
                                    source_street, source_ways)

  @staticmethod
  def generate_straight_connections_to_the_left(entrance_direction, exit_direction,
                          destination_street, destination_ways,
                          source_street, source_ways):
    if entrance_direction == CardinalDirection.WEST or entrance_direction == CardinalDirection.SOUTH:
      destination_arrangement_to_roadway = source_arrangement_to_roadway = ArrangementComparative.FIRST
    else:
      destination_arrangement_to_roadway = source_arrangement_to_roadway = ArrangementComparative.AFTER

    return ConnectionsGenerator \
      .generate_connections_for_lanes_of_ways_arranged_to_roadway(entrance_direction, exit_direction,
                                    destination_arrangement_to_roadway,
                                    source_arrangement_to_roadway,
                                    destination_street, destination_ways,
                                    source_street, source_ways)

  @staticmethod
  def generate_straight_connections_to_the_right(entrance_direction, exit_direction,
                           destination_street, destination_ways,
                           source_street, source_ways):
    if entrance_direction == CardinalDirection.EAST or entrance_direction == CardinalDirection.NORTH:
      destination_arrangement_to_roadway = source_arrangement_to_roadway = ArrangementComparative.FIRST
    else:
      destination_arrangement_to_roadway = source_arrangement_to_roadway = ArrangementComparative.AFTER

    return ConnectionsGenerator \
      .generate_connections_for_lanes_of_ways_arranged_to_roadway(entrance_direction, exit_direction,
                                    destination_arrangement_to_roadway,
                                    source_arrangement_to_roadway,
                                    destination_street, destination_ways,
                                    source_street, source_ways)

  @staticmethod
  def generate_connections_for_lanes_of_ways_arranged_to_roadway(entrance_direction, exit_direction,
                                   destination_arrangement_to_roadway,
                                   source_arrangement_to_roadway,
                                   destination_street, destination_ways,
                                   source_street, source_ways):
    destination_roadway_ordinal = destination_street.get_roadway_ordinal()
    if destination_arrangement_to_roadway == ArrangementComparative.FIRST:
      filtered_destination_ways = ConnectionsGenerator.get_ways_first_roadway(destination_ways,
                                          destination_roadway_ordinal)
    else:
      filtered_destination_ways = ConnectionsGenerator.get_ways_after_roadway(destination_ways,
                                          destination_roadway_ordinal)

    source_roadway_ordinal = source_street.get_roadway_ordinal()
    if source_arrangement_to_roadway == ArrangementComparative.FIRST:
      filtered_source_ways = ConnectionsGenerator.get_ways_first_roadway(source_ways,
                                         source_roadway_ordinal)
    else:
      filtered_source_ways = ConnectionsGenerator.get_ways_after_roadway(source_ways,
                                         source_roadway_ordinal)

    return ConnectionsGenerator.generate_lane_connections(entrance_direction, exit_direction,
                                filtered_source_ways,
                                destination_street.id,
                                filtered_destination_ways)

  @staticmethod
  def get_ways_first_roadway(ways, roadway_ordinal):
    return filter(lambda way: way.ordinal < roadway_ordinal, ways)

  @staticmethod
  def get_ways_after_roadway(ways, roadway_ordinal):
    return filter(lambda way: way.ordinal > roadway_ordinal, ways)
