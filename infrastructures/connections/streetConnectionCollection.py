import json


class StreetConnectionCollection:

    def __init__(self, street_id):
        self.street_id = street_id
        self.connections = {}

    def add_connections(self, connections):
        for connection in connections:
            source_lane_id = connection.source_lane_id
            if self.connections.keys().__contains__(source_lane_id):
                self.connections.get(source_lane_id)\
                    .add_destinations(connection.destinations)
            else:
                self.connections[source_lane_id] = connection

    def serialize_connections(self):
        connections_json = []

        for connection in self.connections.values():
            connections_json.append(json.loads(connection.get_json()))

        return connections_json

    def get_json(self):
        return json.dumps({
            "streetId": self.street_id,
            "connections": self.serialize_connections()
        })