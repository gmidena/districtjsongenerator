from random import randint

from infrastructures.facilities.facility import Facility
from utils.id.facilityIdGenerator import FacilityIdGenerator


class FacilityGenerator:
    def __init__(self):
        pass

    @staticmethod
    def generate(street, street_length, direction):
        facility_stretches = []
        possible_stretches = range(street_length)
        for i in range(street_length // 2):
            index = randint(0, len(possible_stretches)-1)
            facility_stretches.append(possible_stretches[index])
            possible_stretches.pop(index)
        for stretch in facility_stretches:
            facility_id = FacilityIdGenerator.generate()
            garage_capacity = \
                FacilityGenerator.generate_garage_capacity()
            new_facility = Facility(facility_id, garage_capacity)
            street.add_facility(stretch, direction, facility_id)
            street.district.add_facility(new_facility)

    @staticmethod
    def has_facility():
        return randint(0, 4) > 0

    @staticmethod
    def generate_garage_capacity():
        return randint(10, 20)
