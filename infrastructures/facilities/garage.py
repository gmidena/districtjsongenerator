import json


class Garage:

    def __init__(self, capacity):
        self.capacity = capacity
        self.leavingVehicles = []
        self.parkedVehicles = []
        self.pendingVehicles = []

    def add_bicycle(self, bicycle):
        if self.capacity == len(self.parkedVehicles):
            return False
        self.parkedVehicles.append(bicycle)
        return True

    def add_pvt(self, pvt):
        if self.capacity == len(self.parkedVehicles):
            return False
        self.parkedVehicles.append(pvt)
        return True

    def get_json(self):
        return json.dumps({
            "capacity": self.capacity,
            "leavingVehicles" : self.leavingVehicles,
            "parkedVehicles" : self.parkedVehicles,
            "pendingVehicles" : self.pendingVehicles
        })
