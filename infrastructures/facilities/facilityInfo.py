import random

from utils.randomizer import Randomizer


class FacilityInfo:
    facilities = {}
    # district dumps
    districts = {}

    @classmethod
    def add_facility(cls, facility_id, facility_slice):
        cls.facilities[facility_id] = facility_slice

    @classmethod
    def save_and_reset_district_info(cls, district_id):
        district = {}
        district["id"] = district_id
        district["facilities"] = cls.facilities
        cls.districts[district_id] = district
        cls.facilities = {}

    @classmethod
    def get_facilities_for_district(cls, district_id):
        return cls.districts[district_id]["facilities"]

    @classmethod
    def get_slice_from_different_facility(cls, district_id_arg, facility_id_arg):
        districts = cls.districts
        district_ids = districts.keys()
        district_key = random.choice(district_ids)
        while (districts[district_key] == district_id_arg
               and len(district_ids) > 1):
            district_key = random.choice(district_ids)
        district = districts[district_key]

        facilities = district["facilities"]
        facility_ids = facilities.keys()
        facility_key = random.choice(facility_ids)
        while (facilities[facility_key] == facility_id_arg
               and len(facility_ids) > 1):
            facility_key = random.choice(facility_ids)
        facility = facilities[facility_key]

        return facility
