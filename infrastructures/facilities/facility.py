import json

from active.config.activeInfo import ActiveInfo

from infrastructures.facilities.garage import Garage


class Facility:

    def __init__(self, id, garage_capacity):
        self.id = id
        self.guests = []
        self.garage = Garage(garage_capacity)

    def add_guest(self, guest):
        self.guests.append(guest)
        ActiveInfo.add_pedestrian(guest, self.id)

    def add_bicycle(self, bicycle):
        parked = self.garage.add_bicycle(bicycle)
        if parked:
            ActiveInfo.add_bicycle(bicycle, self.id)
        return parked

    def add_pvt(self, pvt):
        parked = self.garage.add_pvt(pvt)
        if parked:
            ActiveInfo.add_pvt(pvt, self.id)
        return parked

    def get_json(self):
        return json.dumps({
            "id": self.id,
            "guests": self.guests,
            "garage": json.loads(self.garage.get_json())
        })
