from infrastructures.lanes.laneGenerator import LaneGenerator
from infrastructures.ways.way import Way

from utils.id.wayIdGenerator import WayIdGenerator
from utils.straightDirection import StraightDirection
from utils.randomizer import Randomizer

class WayGenerator:
    def __init__(self):
        pass

    STARTING_STRETCHES_COUNT_OFFSET = 25

    @staticmethod
    def generate(ordinal, orientation, length):
        way_id = WayIdGenerator.generate()

        way = Way(way_id, ordinal)

        direct_lane = LaneGenerator.generate_direct_lane(orientation, length)
        way.add_lane(direct_lane)

        inverse_lane = LaneGenerator.generate_inverse_lane(orientation, length)
        way.add_lane(inverse_lane)
        return way
