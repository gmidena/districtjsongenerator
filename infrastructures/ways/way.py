import json

from utils.straightDirection import StraightDirection

from utils.randomizer import Randomizer

class Way:

    def __init__(self, id, ordinal):
        self.id = id
        self.ordinal = ordinal
        self.lanes = []

    def add_lane(self, lane):
        self.lanes.append(lane)

    def add_bus(self, bus):
        index = Randomizer.random(0, len(self.lanes)-1)
        return self.lanes[index].add_bus(bus)

    def get_json(self):
        return json.dumps({
            "id": self.id,
            "ordinal": self.ordinal,
            "lanes": self.get_lanes_json()
        })

    def get_lanes_json(self):
        lanes_json = []

        for lane in self.lanes:
            lanes_json.append(json.loads(lane.get_json()))

        return lanes_json

    def add_facility_with_direction(self, stretch_number, direction, facility_id):
        for lane in self.lanes:
            if lane.direction == direction:
                lane.add_facility(stretch_number, facility_id)

    def add_facility(self, stretch_number, facility_id):
        for lane in self.lanes:
            if lane.direction == StraightDirection.WEST_EAST \
                    or lane.direction == StraightDirection.SOUTH_NORTH:
                stretch_number = len(lane.stretches) - stretch_number - 1
            lane.add_facility(stretch_number, facility_id)

    def add_bus_stop(self, bus_id, stretch_index, bus_track):
        index = Randomizer.random(0, len(self.lanes)-1)
        self.lanes[index].add_bus_stop(bus_id, stretch_index, bus_track)
