import json


class LaneDecorations:

    def __init__(self, speed_limit):
        self.speed_limit = speed_limit

    def get_json(self):
        return json.dumps({
            "speedLimit": self.speed_limit
        })