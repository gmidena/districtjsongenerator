from infrastructures.lanes.lane import Lane
from infrastructures.stretches.stretch import Stretch

from utils.id.laneIdGenerator import LaneIdGenerator
from utils.id.treadableIdGenerator import TreadableIdGenerator
from utils.orientation import Orientation
from utils.randomizer import Randomizer
from utils.straightDirection import StraightDirection


class LaneGenerator:
    def __init__(self):
        pass

    @staticmethod
    def generate(direction, length):
        lane_id = LaneIdGenerator.generate()
        lane_direction = LaneGenerator.get_random_speed_limit_or_none()
        lane = Lane(lane_id, direction, lane_direction)
        LaneGenerator.generate_stretches(lane, length)
        return lane

    @staticmethod
    def generate_direct_lane(orientation, length):
        if orientation == Orientation.HORIZONTAL:
            direction = StraightDirection.WEST_EAST
        else:
            direction = StraightDirection.NORTH_SOUTH

        return LaneGenerator.generate(direction, length)

    @staticmethod
    def generate_inverse_lane(orientation, length):
        if orientation == Orientation.HORIZONTAL:
            direction = StraightDirection.EAST_WEST
        else:
            direction = StraightDirection.SOUTH_NORTH

        return LaneGenerator.generate(direction, length)

    @staticmethod
    def get_random_speed_limit_or_none():
        speed_limits = [None, 50, 60, 70, 80, 90, 100]
        return Randomizer.select_random(speed_limits)

    @staticmethod
    def generate_stretches(lane, length):
        while length > 0:
            stretch_id = TreadableIdGenerator.generate()
            stretch_size = Randomizer.random(5,9)
            stretch = Stretch(stretch_id, stretch_size)
            lane.add_stretch(stretch)
            length = length - 1
