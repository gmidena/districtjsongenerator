import json

from infrastructures.lanes.laneDecorations import LaneDecorations

from utils.randomizer import Randomizer

class Lane:

    def __init__(self, id, direction, speed_limit):
        self.id = id
        self.direction = direction
        self.decorations = LaneDecorations(speed_limit)
        self.stretches = []

    def add_stretch(self, stretch):
        self.stretches.append(stretch)

    def add_bus(self, bus):
        index = Randomizer.random(0, len(self.stretches)-1)
        added_info = self.stretches[index].add_bus(bus)
        if added_info is None:
            return None
        else:
            added_info["direction"] = self.direction
            added_info["stretch_index"] = index
            return added_info

    def get_json(self):
        return json.dumps({
            "id": self.id,
            "direction": self.direction.name,
            "decorations": json.loads(self.decorations.get_json()),
            "stretches": self.serialize_stretches()
        })

    def serialize_stretches(self):
        stretches_json = []

        for stretch in self.stretches:
            stretches_json.append(json.loads(stretch.get_json()))

        return stretches_json

    def add_facility(self, stretch_number, facility_id):
        facility_stretch = self.stretches[stretch_number]
        facility_stretch.set_facility_id(facility_id)

    def add_bus_stop(self, bus_id, stretch_index, bus_track):
        self.stretches[stretch_index].add_bus_stop(bus_id, bus_track)
