import json

from infrastructures.facilities.facilityInfo import FacilityInfo

from utils.straightDirection import StraightDirection
from utils.randomizer import Randomizer

class Street:

    def __init__(self, id, orientation, district):
        self.id = id
        self.orientation = orientation
        self.roadways = []
        self.bikeways = []
        self.footways = []
        self.district = district

    def add_roadway(self, roadway):
        self.roadways.append(roadway)

    def add_bikeway(self, bikeway):
        self.bikeways.append(bikeway)

    def add_footway(self, footway):
        self.footways.append(footway)

    def add_bus(self, bus):
        index = Randomizer.random(0, len(self.roadways)-1)
        return self.roadways[index].add_bus(bus)

    def get_roadway_ordinal(self):
        return self.roadways[0].ordinal

    def get_json(self):
        return json.dumps({
            "id": self.id,
            "orientation": self.orientation.name,
            "roadways": self.get_roadways_json(),
            "bikeways": self.get_bikeways_json(),
            "footways": self.get_footways_json()
        })

    def get_roadways_json(self):
        roadways_json = []

        for roadway in self.roadways:
            roadways_json.append(json.loads(roadway.get_json()))

        return roadways_json

    def get_bikeways_json(self):
        bikeways_json = []

        for bikeway in self.bikeways:
            bikeways_json.append(json.loads(bikeway.get_json()))

        return bikeways_json

    def get_footways_json(self):
        footways_json = []

        for footway in self.footways:
            footways_json.append(json.loads(footway.get_json()))

        return footways_json

    def add_facility(self, stretch_number, direction, facility_id):
        #  there is only one roadway (default)
        roadway = self.roadways[0]
        ordinal_bound = roadway.ordinal

        roadway.add_facility_with_direction(stretch_number,
                                            direction,
                                            facility_id)

        facility_slice = self.get_ways_for_direction(direction)
        for way in facility_slice["bike"]:
            way.add_facility(stretch_number, facility_id)
        for way in facility_slice["foot"]:
            way.add_facility(stretch_number, facility_id)

        FacilityInfo.add_facility( \
            facility_id,
            self.get_slice(direction, stretch_number))


    def extract_ways(self):
        ways = {}

        for roadway in self.roadways:
            ways[roadway.ordinal] = roadway
        for bikeway in self.bikeways:
            ways[bikeway.ordinal] = bikeway
        for footway in self.footways:
            ways[footway.ordinal] = footway

        return ways


    def get_ways_for_direction(self, direction):
        #  there is only one roadway (default)
        roadway = self.roadways[0]
        ordinal_bound = roadway.ordinal
        if direction == StraightDirection.NORTH_SOUTH or \
           direction == StraightDirection.EAST_WEST:
            fun = lambda x,y: x < y
        else:
            fun = lambda x,y: x > y
        ways_in_slice = { "bike" : [], "foot": [], "road": [roadway]}
        for bikeway in self.bikeways:
            if fun(bikeway.ordinal, ordinal_bound):
                ways_in_slice["bike"].append(bikeway)
        for footway in self.footways:
            if fun(footway.ordinal, ordinal_bound):
                ways_in_slice["foot"].append(footway)
        return ways_in_slice

    def get_slice(self, direction, stretch_index):
        ways_for_direction = self.get_ways_for_direction(direction)
        result_slice = { "bike" : [ ], "foot": [ ], "road": [ ]}
        for roadway in ways_for_direction["road"]:
            for lane in roadway.lanes:
                result_slice["road"].append(lane.stretches[stretch_index])
        for bikeway in ways_for_direction["bike"]:
            for lane in bikeway.lanes:
                result_slice["bike"].append(lane.stretches[stretch_index])
        for footway in ways_for_direction["foot"]:
            for lane in footway.lanes:
                result_slice["foot"].append(lane.stretches[stretch_index])
        return result_slice

    def get_slice_without_bus_stop(self):
        roadway = self.roadways[0]
        lane_index = Randomizer.random(0, len(roadway.lanes)-1)
        roadway_lane = roadway.lanes[lane_index]
        stretch_index = Randomizer.random(0, len(roadway_lane.stretches)-1)
        roadway_stretch = roadway_lane.stretches[stretch_index]
        if roadway_stretch.has_bus_stop():
            return None
        return self.get_slice(roadway_lane.direction, stretch_index)
