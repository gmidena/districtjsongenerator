from infrastructures.facilities.facilityGenerator import FacilityGenerator
from infrastructures.streets.street import Street
from infrastructures.ways.wayGenerator import WayGenerator

from utils.cardinalDirection import CardinalDirection
from utils.straightDirection import StraightDirection
from utils.id.idGenerator import IdGenerator
from utils.orientation import Orientation
from utils.randomizer import Randomizer


class StreetGenerator:
    def __init__(self):
        pass

    @staticmethod
    def generate(street_id, orientation, length, district):
        street = Street(street_id, orientation, district)
        ordinal_generator = IdGenerator()

        ordinal = ordinal_generator.generate()
        footway = WayGenerator.generate(ordinal, orientation, length)
        street.add_footway(footway)

        ordinal = ordinal_generator.generate()
        bikeway = WayGenerator.generate(ordinal, orientation, length)
        street.add_bikeway(bikeway)

        ordinal = ordinal_generator.generate()
        roadway = WayGenerator.generate(ordinal, orientation, length)
        street.add_roadway(roadway)

        ordinal = ordinal_generator.generate()
        bikeway = WayGenerator.generate(ordinal, orientation, length)
        street.add_bikeway(bikeway)

        ordinal = ordinal_generator.generate()
        footway = WayGenerator.generate(ordinal, orientation, length)
        street.add_footway(footway)

        StreetGenerator.generate_facilities(street, length)

        StreetGenerator.generate_crossing(street, length)
        return street

    @staticmethod
    def generate_facilities(street, street_length):
        if street.orientation == Orientation.HORIZONTAL:
            FacilityGenerator.generate(street,
                                       street_length,
                                       StraightDirection.EAST_WEST)
            FacilityGenerator.generate(street,
                                       street_length,
                                       StraightDirection.WEST_EAST)
        else:
            FacilityGenerator.generate(street,
                                       street_length,
                                       StraightDirection.NORTH_SOUTH)
            FacilityGenerator.generate(street,
                                       street_length,
                                       StraightDirection.SOUTH_NORTH)

    @staticmethod
    def generate_crossing(street, stretches_count):
      index_limit = stretches_count - 1
      def_direction = street.roadways[0].lanes[0].direction
      ped0 = Randomizer.random(1, index_limit - 2)
      ped_crossing_indexes = [ ped0 ]
      bike_crossing_indexes = [ ]

      bike_crossing_indexes.append(ped0 - 1)
      ped_crossing_indexes.append(ped0 + 1)
      bike_crossing_indexes.append(ped0 + 2)

      for ped_index in ped_crossing_indexes:
        complementary_index = index_limit - ped_index
        for way in street.roadways:
          for lane in way.lanes:
            if lane.direction == def_direction:
              lane.stretches[ped_index].set_pedestrian_crossing(True)
            else:
              lane.stretches[complementary_index].set_pedestrian_crossing(True)
        for way in street.bikeways:
          for lane in way.lanes:
            if lane.direction == def_direction:
              lane.stretches[ped_index].set_pedestrian_crossing(True)
            else:
              lane.stretches[complementary_index].set_pedestrian_crossing(True)
        for way in street.footways:
          for lane in way.lanes:
            if lane.direction == def_direction:
              lane.stretches[ped_index].set_pedestrian_crossing(True)
            else:
              lane.stretches[complementary_index].set_pedestrian_crossing(True)

      for bike_index in bike_crossing_indexes:
        complementary_index = index_limit - bike_index
        for way in street.roadways:
          for lane in way.lanes:
            if lane.direction == def_direction:
              lane.stretches[bike_index].set_bicycle_crossing(True)
            else:
              lane.stretches[complementary_index].set_bicycle_crossing(True)
        for way in street.bikeways:
          for lane in way.lanes:
            if lane.direction == def_direction:
              lane.stretches[bike_index].set_bicycle_crossing(True)
            else:
              lane.stretches[complementary_index].set_bicycle_crossing(True)
        for way in street.footways:
          for lane in way.lanes:
            if lane.direction == def_direction:
              lane.stretches[bike_index].set_bicycle_crossing(True)
            else:
              lane.stretches[complementary_index].set_bicycle_crossing(True)
