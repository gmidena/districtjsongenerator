from os import listdir
from os.path import isfile, join
import sys
import json
import re

def help_me():
   print "USAGE"
   print "\t python "+sys.argv[0]+" <number_of_pedestrian_for_district> "+" <output_folder> "
   print "\t Example:"
   print "\t\t python "+sys.argv[0]+" 100 "+" resource/input/12/active "
   print "INFO"
   print "\tScript which generates input for active based on the existing district input."
   print "\tIt calculates the number of buses, bicycles and private motor vehicles based on the number of pedestrians."

def generate_config(city_name, number_of_pedestrians, output_dir):
  config = {
            "speeds" : {
                 "absolute" : 100,
                 "PEDESTRIAN" : 5,
                 "BICYCLE" : 20,
                 "BUS" : 80,
                 "CAR" : 100,
                 "MOTORCYCLE" : 70,
                 "SIDECAR" : 60
            },
            "passengers" : {
                 "BICYCLE" : 2,
                 "BUS" : 6,
                 "CAR" : 4,
                 "MOTORCYCLE" : 1,
                 "SIDECAR" : 2
            },
            "trafficLightPeriod" : 60
         }
  return (config, str(city_name), int(number_of_pedestrians), output_dir)

def compute_active(number_of_pedestrians, total_buses_no):
  percentage     = number_of_pedestrians // 100
  bicycles_no    = percentage * 20
  buses_no       = percentage * 2
  pvts_no        = percentage * 80
  total_buses_no = total_buses_no + buses_no
  if total_buses_no > 50:
    total_buses_no = total_buses_no - buses_no + 1
    buses_no = 1
  active = {
    "pedestrians_no" : number_of_pedestrians,
    "bicycles_no"    : bicycles_no,
    "buses_no"       : buses_no,
    "pvts_no"        : pvts_no
  }
  return (active, total_buses_no)

def generate_active(data):
  config       = data[0]
  city_name    = data[1]
  pedestrians  = data[2]
  output_dir   = data[3]
  total_travellers_no = 0
  district_dir = output_dir.replace("active","districts")
  files = [f for f in listdir(district_dir) if isfile(join(district_dir, f))]
  files = [f for f in files if f[len(f)-5:len(f)] == ".json"]
  files = [f.replace("districts","active") for f in files]
  actives = []
  total_buses_no = 0
  for f in files:
    (active, total_buses_no) = compute_active(pedestrians, total_buses_no)
    total_travellers_no += active["pedestrians_no"] + active["bicycles_no"] + \
    + active["buses_no"] + active["pvts_no"]
    actives.append(active)
  print "NUMBER OF BUSES : " + str(total_buses_no)
  return (city_name, config, actives, output_dir, files, total_travellers_no)

def write_data(data):
  city_name = data[0]
  config    = data[1]
  actives   = data[2]
  out       = data[3]
  files     = data[4]
  agents_no = data[5]
  input_data = {}
  with open(out + "config_active.json", "w+") as outfile:
    json.dump(config, outfile, indent=4)
  for i in range(0, len(files)):
    with open(out + files[i], "w+") as outfile:
       json.dump(actives[i], outfile, indent=4)
  ai_file = "resource/output/ai/ai" + city_name + ".conf"
  input_data["agents_no"] = int(agents_no)
  input_data["path"] = "/var/snapshot/ai/" + city_name
  input_data["ext"] = ".json"
  with open(ai_file, "w+") as outfile:
    json.dump(input_data, outfile, indent=4)

if(len(sys.argv) != 4):
  print 'Invalid parameters'
  help_me()
  sys.exit()

write_data( \
  generate_active( \
    generate_config( \
      sys.argv[1], sys.argv[2], sys.argv[3])))
