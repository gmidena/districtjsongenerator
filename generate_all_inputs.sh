#!/bin/bash

function generate_districts
{
   streets=${1:-}
   max_size=${2:-}
   out_path=${3:-}
   mw_path=${4:-}
   name=${5:-}
   python generate_district_input.py $streets $max_size $out_path $mw_path $name
}


function generate_active
{
   city_name=${1:-}
   ped_for_district=${2:-}
   out_path=${3:-}
   python generate_active_input.py $city_name $ped_for_district $out_path
}

function generate_bus_map
{
   number_of_buses=${1:-}
   out_path=${2:-}
   python generate_bus_input.py $number_of_buses  $out_path
}

function main
{
   name=${1:-}
   city=${2:-}
   length=${3:-}

   max_street_size=100
   pedestrians_district=100

   out_prefix="resource/input/"
   mw_prefix="resource/mw_config/"
   mw_suffix="routing/"
   district_pref="districts/"
   active_pref="active/"
   bus_pref="bus_map/"

   district_path=$out_prefix$name"/"$district_pref
   active_path=$out_prefix$name"/"$active_pref
   bus_path=$out_prefix$name"/"$bus_pref
   mw_path=$mw_prefix$name"/"$mw_suffix

   generate_districts $city $length $district_path $mw_path $name
   num_of_buses=$(generate_active $name $pedestrians_district $active_path | cut -f2 -d: | cut -f2 -d' ')
   generate_bus_map $num_of_buses $bus_path

   echo "CITY-ID : "$name
   echo "NUMBER OF TOTAL STREETS : "$city
   echo "NUMBER OF PEDESTRIANS FOR DISTRICT : "$pedestrians_district
   echo "NUMBER OF TOTAL BUSES : "$num_of_buses
}

main $1 $2 $3
