from active.config.activeInfo import ActiveInfo

from bus.trackInfo import TrackInfo

from .district import District

from infrastructures.connections.laneConnectionCollection \
    import LaneConnectionCollection
from infrastructures.connections.streetConnectionCollection \
    import StreetConnectionCollection
from infrastructures.connections.connectionsGenerator \
    import ConnectionsGenerator
from infrastructures.streets.streetGenerator import StreetGenerator
from infrastructures.intersections.intersectionGenerator \
    import IntersectionGenerator

from utils.id.agentIdGenerator import AgentIdGenerator
from utils.cardinalDirection import CardinalDirection
from utils.orientation import Orientation
from utils.directionUtil import DirectionUtil


class DistrictGenerator:
    def __init__(self):
        pass

    @staticmethod
    def generate_streets(district_requirements, bus_map):
        district = District()

        streets_requirements = district_requirements["streets"]
        streets = {}
        for street in streets_requirements:
            street_id = street["id"]
            street_orientation = Orientation[street["orientation"]]
            street_length = street["length"]
            streets[street_id] = StreetGenerator.generate(street_id,
                                                          street_orientation,
                                                          street_length,
                                                          district)
            district.add_street(streets[street_id])

        # Add active entities refs here and there

        # Get the next number in a sliding window of dimension max with window
        # size = 1
        def get_next_number_in_window(current, max):
            current = current +1
            if current >= max:
                current = 0
            return current

        facility_counter = 0
        facility_no      = len(district.facilities)
        for i in range(ActiveInfo.get_pedestrians_no()):
            # Generate pedestrian id and add it to a random facility
            next_active_id = AgentIdGenerator.generate()
            district.add_guest_to_facility(facility_counter, next_active_id)
            facility_counter = \
                get_next_number_in_window(facility_counter, facility_no)

        for i in range(ActiveInfo.get_bicycles_no()):
            attempts_counter = 0
            # Generate bicycle id and add it to a random facility
            next_active_id = AgentIdGenerator.generate()
            added = False
            while not added:
                added = district.add_bicycle_to_facility(facility_counter,
                                                         next_active_id)
                if not added:
                    facility_counter = \
                        get_next_number_in_window(facility_counter, facility_no)
                    attempts_counter = attempts_counter + 1
                    if attempts_counter > facility_counter:
                        raise Exception("Too many vehicles")
            facility_counter = \
                get_next_number_in_window(facility_counter, facility_no)

        for i in range(ActiveInfo.get_pvts_no()):
            attempts_counter = 0
            # Generate pvt id and add it to a random facility
            next_active_id = AgentIdGenerator.generate()
            added = False
            while not added:
                added = district.add_pvt_to_facility(facility_counter,
                                                         next_active_id)
                if not added:
                    facility_counter = \
                        get_next_number_in_window(facility_counter, facility_no)
                    attempts_counter = attempts_counter + 1
                    if attempts_counter > facility_counter:
                        raise Exception("Too many vehicles")
            facility_counter = \
                get_next_number_in_window(facility_counter, facility_no)

        buses_no = ActiveInfo.get_buses_no()
        stops_added = 0
        first_stretches = []
        for i in range(buses_no):
            bus_id = TrackInfo.extract_bus()
            bus_track = bus_map[bus_id]
            added_info = None
            while added_info is None:
                added_info = district.add_bus(bus_id)
            # Add bus stops for bus_id
            bus_slice = district.get_slice( \
                added_info["street"],
                added_info["direction"],
                added_info["stretch_index"])
            ActiveInfo.add_bus(bus_id, added_info["roadway_stretch"].id)
            first_stretches.append(added_info["roadway_stretch"].id)
            added_info["bus_stop_id"] = bus_track[0]
            DistrictGenerator.add_bus_stop(bus_slice, added_info, bus_map)
            if TrackInfo.remove_bus_stop_id(bus_track[0]):
                stops_added = stops_added + 1
        stops_per_districts_no = TrackInfo.get_stops_per_district_no()
        # Generate (bus_stops_no_per_district - buses_no) bus stops
        for i in range(stops_per_districts_no - stops_added):
            slice_without_bus_stop = None
            while slice_without_bus_stop is None:
                slice_without_bus_stop = \
                    district.get_slice_without_bus_stop(first_stretches)
            bus_stop_id = TrackInfo.extract_bus_stop_id()
            bus_info = {}
            bus_info["bus_stop_id"] = bus_stop_id
            bus_info["roadway_stretch"] = slice_without_bus_stop["road"][0]
            first_stretches.append(bus_info["roadway_stretch"].id)
            DistrictGenerator.add_bus_stop(slice_without_bus_stop,
                                           bus_info,
                                           bus_map)

        return district

    @staticmethod
    def generate_intersections(district_requirements, district, streets):
        intersections_requirements = district_requirements["intersections"]
        for intersection_requirements in intersections_requirements:
            intersection_exits_requirements = intersection_requirements["exits"]
            intersection = IntersectionGenerator.generate()
            exit_to_street_map = {}
            for exit in intersection_exits_requirements:
                street_id = exit["streetId"]
                exit_direction = CardinalDirection[exit["direction"]]
                exit_to_street_map[exit_direction] = street_id
            if len(exit_to_street_map.keys()) == 2:
                intersection = \
                    DistrictGenerator.generate_intersection_two( \
                        streets, exit_to_street_map, intersection)
                district.add_intersection(intersection)
                connections = \
                    DistrictGenerator.generate_connections_two( \
                        streets, intersection)
                district.add_connections(connections)
                continue
            for exit_direction in \
                    DistrictGenerator.sort_exits(exit_to_street_map.keys()):
                street_id = exit_to_street_map[exit_direction]
                traffic_light_id = AgentIdGenerator.generate()
                intersection.connect_street(streets[street_id],
                                            exit_direction,
                                            traffic_light_id)
                ActiveInfo.add_traffic_light(traffic_light_id)
            district.add_intersection(intersection)
            connections = ConnectionsGenerator.generate(intersection, streets)
            district.add_connections(connections)
        return district


    @staticmethod
    def sort_exits(exits):
        if len(exits) == 2:
            return exits
        if len(exits) == 4:
            return [CardinalDirection["EAST"],
                    CardinalDirection["NORTH"],
                    CardinalDirection["WEST"],
                    CardinalDirection["SOUTH"]]
        elif CardinalDirection["EAST"] not in exits:
            return [CardinalDirection["NORTH"],
                    CardinalDirection["WEST"],
                    CardinalDirection["SOUTH"]]
        elif CardinalDirection["WEST"] not in exits:
            return [CardinalDirection["NORTH"],
                    CardinalDirection["EAST"],
                    CardinalDirection["SOUTH"]]
        elif CardinalDirection["NORTH"] not in exits:
            return [CardinalDirection["EAST"],
                    CardinalDirection["SOUTH"],
                    CardinalDirection["WEST"]]
        else:
            return [CardinalDirection["EAST"],
                    CardinalDirection["NORTH"],
                    CardinalDirection["WEST"]]

    @staticmethod
    def generate_intersection_two(streets, exits, intersection):
        directions = exits.keys()
        street_0 = exits[directions[0]]
        intersection.connect_street(streets[street_0],
                                    directions[0],
                                    None)
        street_1 = exits[directions[1]]
        intersection.connect_street(streets[street_1],
                                    directions[1],
                                    None)
        return intersection

    @staticmethod
    def generate_connections_two(streets, intersection):
        connections = []
        exits = intersection.exits
        directions  = exits.keys()
        street_0_id = exits[directions[0]].street_id
        street_1_id = exits[directions[1]].street_id
        street_0    = streets[street_0_id]
        street_1    = streets[street_1_id]
        connections.append( \
            DistrictGenerator.generate_connections_for_corner( \
                street_0, street_1, directions[0], directions[1]))
        connections.append( \
            DistrictGenerator.generate_connections_for_corner( \
                street_1, street_0, directions[1], directions[0]))
        return connections

    @staticmethod
    def generate_connections_for_corner(from_str, to_str, from_dir, to_dir):
        street_connection = StreetConnectionCollection(from_str.id)
        pairs = []

        if DistrictGenerator.same_ordinal(from_dir, to_dir):
            pairs.append( \
                {"from": from_str.footways[0], "to": to_str.footways[0]})
            pairs.append( \
                {"from": from_str.footways[1], "to": to_str.footways[1]})
            pairs.append( \
                {"from": from_str.bikeways[0], "to": to_str.bikeways[0]})
            pairs.append( \
                {"from": from_str.bikeways[1], "to": to_str.bikeways[1]})
        else:
            pairs.append( \
                {"from": from_str.footways[0], "to": to_str.footways[1]})
            pairs.append( \
                {"from": from_str.footways[1], "to": to_str.footways[0]})
            pairs.append( \
                {"from": from_str.bikeways[0], "to": to_str.bikeways[1]})
            pairs.append( \
                {"from": from_str.bikeways[1], "to": to_str.bikeways[0]})

        pairs.append({"from": from_str.roadways[0], "to": to_str.roadways[0]})
        lane_connections = []
        for pair in pairs:
            way_from = pair["from"]
            way_to   = pair["to"]
            lane_from = None
            for lane in way_from.lanes:
                if DirectionUtil.get_source(lane.direction) == from_dir:
                    lane_from = lane
            lane_to = None
            for lane in way_to.lanes:
                if DirectionUtil.get_destination(lane.direction) == to_dir:
                    lane_to = lane
            if lane_from != None and lane_to != None:
                lane_connection = LaneConnectionCollection(lane_from.id)
                lane_connection.add_destination(to_str.id, lane_to.id, way_to.id)
                lane_connections.append(lane_connection)
        street_connection.add_connections(lane_connections)
        return street_connection

    @staticmethod
    def same_ordinal(dir1, dir2):
        east  = CardinalDirection["EAST"]
        south = CardinalDirection["SOUTH"]
        north = CardinalDirection["NORTH"]
        west  = CardinalDirection["WEST"]
        if dir1 == east  and dir2 == south:
            return True
        if dir1 == south and dir2 == east:
            return True
        if dir1 == west  and dir2 == north:
            return True
        if dir1 == north and dir2 == west:
            return True
        return False

    @staticmethod
    def add_bus_stop(bus_slice, bus_info, bus_map):
        bus_stop_id = bus_info["bus_stop_id"]

        bus_tracks = { }
        for bus_id in bus_map.keys():
            bus_track = bus_map[bus_id]
            if bus_stop_id in bus_track:
                bus_tracks[bus_id] = bus_track

        footway_stretch = bus_slice["foot"][0]
        roadway_stretch = bus_info["roadway_stretch"]

        TrackInfo.add_route_stop(roadway_stretch.id, bus_stop_id)

        footway_stretch.change_id(bus_stop_id)
        for bus_id in bus_tracks.keys():
            bus_track = bus_tracks[bus_id]
            footway_stretch.add_bus_stop(bus_id, bus_track)
            # CAUTION - little efficiency trick
            # bus stops are not needed for bus stops on roadway
            roadway_stretch.add_bus_stop(bus_id, [])
