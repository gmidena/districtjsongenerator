import json

from utils.randomizer import Randomizer

class District:

    def __init__(self):
        self.streets = []
        self.intersections = []
        self.facilities = []
        self.connections = []

    def add_street(self, street):
        self.streets.append(street)

    def add_intersection(self, intersection):
        self.intersections.append(intersection)

    def add_facility(self, facility):
        self.facilities.append(facility)

    def add_bus(self, bus):
        added_info = None
        while added_info is None:
            index = Randomizer.random(0, len(self.streets)-1)
            added_info = self.streets[index].add_bus(bus)
        added_info["street"] = index
        return added_info

    def add_connections(self, connections):
        self.connections.extend(connections)

    def add_guest_to_facility(self, facility_index, guest):
        self.facilities[facility_index].add_guest(guest)

    def add_bicycle_to_facility(self, facility_index, bicycle):
        return self.facilities[facility_index].add_bicycle(bicycle)

    def add_pvt_to_facility(self, facility_index, pvt):
        return self.facilities[facility_index].add_pvt(pvt)

    def get_slice(self, street_index, direction, stretch_index):
        return self.streets[street_index].get_slice(direction, stretch_index)

    def get_slice_without_bus_stop(self, already_present):
        index = Randomizer.random(0, len(self.streets)-1)
        bus_info = self.streets[index].get_slice_without_bus_stop()
        while (bus_info is not None) and (bus_info["road"][0].id in already_present):
            index = Randomizer.random(0, len(self.streets)-1)
            bus_info = self.streets[index].get_slice_without_bus_stop()
        return bus_info

    @staticmethod
    def remove_nulls(d):
        return {k: v for k, v in d.iteritems() if v is not None}

    def get_json(self):
        json_result_str = \
            json.dumps({
                "streets": self.get_streets_json(),
                "intersections": self.get_intersections_json(),
                "facilities": self.get_facilities_json(),
                "connections": self.get_connections_json()
            })
        json_result = json.loads(json_result_str,
                                 object_hook=District.remove_nulls)
        return json.dumps(json_result, indent=2, sort_keys=True)

    def get_streets_json(self):
        streets_json = []

        for street in self.streets:
            streets_json.append(json.loads(street.get_json()))

        return streets_json

    def get_intersections_json(self):
        intersections_json = []

        for intersection in self.intersections:
            intersections_json.append(json.loads(intersection.get_json()))

        return intersections_json

    def get_facilities_json(self):
        facilities_json = []

        for facility in self.facilities:
            facilities_json.append(json.loads(facility.get_json()))

        return facilities_json

    def get_connections_json(self):
        connections_json = []

        for connection in self.connections:
            connections_json.append(json.loads(connection.get_json()))

        return connections_json
