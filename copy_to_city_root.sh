city=${1:-}                # City identifier
in_info=${2:-}             # District information folder
in_mw=${3:-}               # Middleware configuration files folder
in_ai=${4:-}               # AI configuration files folder
in_interface_layer=${5:-}  # Interface layer configuration files folder
in_collected=${6:-}        # Collected info *file*
cities_array=${7:-}        # Collected cities for broker

out_info=$CITY_ROOT"/app/backend/etc/init/"
out_fe=$CITY_ROOT"/app/frontend/etc"
out_collected=$CITY_ROOT"/app/backend/var/snapshot/$city"
out_broker_dev=$CITY_ROOT"/broker_to_as/config/dev.exs"
out_broker_prod=$CITY_ROOT"/broker_to_as/config/prod.exs"

# 0) Create output folders, if not present
mkdir -p $out_fe/$city
chmod -R 666 $out_fe/$city
mkdir -p $out_collected
chmod -R 666 $out_collected

# 1) Copy districts information {active, agendas, reactive, ai, interface_layer}
in_active=(`ls $in_info/active`)
out_active=(`ls $in_info/active | perl -pe 's/out-/active/' | perl -pe 's/json/conf/'`)
for i in "${!in_active[@]}"
do
  cp $in_info"active/"${in_active[i]} $out_info${out_active[i]}
  cp $in_info"active/"${in_active[i]} $out_fe/$city/${out_active[i]}
done

in_agendas=(`ls $in_info/agendas`)
out_agendas=(`ls $in_info/agendas | perl -pe 's/agenda-/scheduling/' | perl -pe 's/json/conf/'`)
for i in "${!in_agendas[@]}"
do
  cp $in_info"agendas/"${in_agendas[i]} $out_info${out_agendas[i]}
done

in_districts=(`ls $in_info/districts`)
out_districts=(`ls $in_info/districts | perl -pe 's/out-/district/' | perl -pe 's/json/conf/'`)
for i in "${!in_districts[@]}"
do
  cp $in_info"districts/"${in_districts[i]} $out_info${out_districts[i]}
  cp $in_info"districts/"${in_districts[i]} $out_fe/$city/${out_districts[i]}
done

ai_configs=(`ls $in_ai`)
for config in "${ai_configs[@]}"
do
  cp $in_ai$config $out_info$config
done

interface_layer_configs=(`ls $in_interface_layer`)
for config in "${interface_layer_configs[@]}"
do
  cp $in_interface_layer$config $out_info$config
done


# 2) Copy middleware information
out_mw=$CITY_ROOT/mw
echo $in_mw
mkdir -p $out_mw/apps/naming/etc/$city
cp $in_mw/names.conf $out_mw/apps/naming/etc/$city/entities.conf
districts_nos=(`ls $in_info/districts | perl -pe 's/out-//' | perl -pe 's/json/conf/'`)
for i in "${!districts_nos[@]}"
do
  cp $in_mw/routing/${districts_nos[i]} $out_mw/apps/utils/etc/${districts_nos[i]}
done


# 3) Copy collected information
cp $in_collected $out_collected/"district.conf"

# 4) Copy broker information
cities_config="config :broker_to_as, cities: $cities_array"
if grep -q "cities" $out_broker_dev; then
  sed -i.bak "s/.*cities.*/$cities_config/" $out_broker_dev
  sed -i.bak "s/.*cities.*/$cities_config/" $out_broker_prod
else
  echo $cities_config >> $out_broker_dev
  echo $cities_config >> $out_broker_prod
fi
