#!/bin/bash


input=${1:-resource}
output=${2:-resource}

echo "Cleaning $input ..."

find $input -maxdepth 3 -type f -delete

echo "Cleaning $output ..."
find $output -maxdepth 3 -type f -delete

echo "Cleaning $CITY_ROOT ..."
rm -f $CITY_ROOT/app/backend/etc/init/*
rm -rf $CITY_ROOT/app/frontend/etc/*
rm -rf $CITY_ROOT/var/snapshot/*
rm -rf $CITY_ROOT/mw/apps/naming/etc/*
rm -rf $CITY_ROOT/mw/apps/utils/etc/*
