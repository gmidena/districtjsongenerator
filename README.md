# Slartibartfast - Generator for simulator configuration files #

## Usage ##

    python <root_folder> <path/to/input/folder> <path/to/output/folder>

or

    python3 <root_folder> <path/to/input/folder> <path/to/output/folder>

Where:

* `<root_folder>`: path to the root of this repository.
  If you're running the command from the folder this README.md file is placed,
  then just substitute `<root_folder>` with `.`
* `<path/to/input/folder>` (optional): path to input directory. This folder
  will contain configuration files for active entities, buses and
  infrastructure placed respectively in the folders `active/`, `bus_map/` and
  `districts/`
* `<path/to/output/folder>` (optional): path to output directory. This folder
  will contain three subfolders `active/`, `agendas/` and `districts/` in which
  the generator will output the configuration files for the simulator


## JSON input scheme for infrastructure ##

```json
{
  "streets": [{
    "id": <number>,
    "length": <number>,
    "orientation": <HORIZONTAL|VERTICAL>
  }, ...],
  "intersections": [{
    "exits": [{
      "streetId": <number>,
          "direction": <WEST|SOUTH|EAST|NORTH>
    }, ...]
  }, ...]
}
```

### JSON input example ###
An example of this type of JSON input is present in the
`resource/input/districts` directory.

### Python's supported versions ###
The support for Python is guaranteed from 2.7 version onwards.

If you use Python 2.x, you have to install `enum34`, e.g. using the command

    pip install enum34
