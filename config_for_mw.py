# Generates configuration files for the district, so the middleware can tell
# in which district there is a given reactive entity

import json
import sys

from utils.fileWriter import FileWriter
from utils.jsonFileLoader import JsonFileLoader

if len(sys.argv) <= 3:
	print("Usage: " + sys.argv[0]
			+ " <input_filename> <output_directory> <output_filename>")
	sys.exit()

input_file = sys.argv[1]
print("Input file is " + input_file)

output_dir  = sys.argv[2]
output_file = sys.argv[3]
print("You will find the output in " + output_dir + output_file)


def get_stretch_ids_from_lane(lane):
	stretches = lane["stretches"]
	return map(lambda x: x["id"], stretches)

def get_stretch_ids_from_way(way):
	stretch_list = []
	lanes = way["lanes"]
	return \
		reduce(
			lambda x,y: x + y,
			map(
				lambda lane: get_stretch_ids_from_lane(lane),
				lanes
			)
		)

def get_stretch_ids(street):
	bikeways = street["bikeways"]
	footways = street["footways"]
	roadways = street["roadways"]
	return \
		reduce(
			lambda x,y: x + y,
			map(
				lambda way: get_stretch_ids_from_way(way),
				bikeways
			)
		)    \
		+    \
		reduce(
			lambda x,y: x + y,
			map(
				lambda way: get_stretch_ids_from_way(way),
				footways
			)
		)    \
		+    \
		reduce(
			lambda x,y: x + y,
			map(
				lambda way: get_stretch_ids_from_way(way),
				roadways
			)
		)


node_ids = []
treadables = {}
hosts      = {}

print(input_file)
input_json = JsonFileLoader.load(input_file)
nodes = input_json["nodes"]
for node in nodes:
	node_id = node.keys()[0]
	node_ids.append(node_id)
	node_info = node[node_id]

	stretch_ids = []
	node_streets = node_info["streets"]
	for street in node_streets:
		stretch_ids.extend(get_stretch_ids(street))
	treadables[node_id] = stretch_ids

	intersection_ids = []
	node_intersections = node_info["intersections"]
	for intersection in node_intersections:
		intersection_ids.append(intersection["id"])
	treadables[node_id].extend(intersection_ids)

	facility_ids = []
	node_facilities = node_info["facilities"]
	for facility in node_facilities:
		facility_ids.append(facility["id"])
	hosts[node_id] = facility_ids


def get_json_for_single(node_id):
	return \
		json.dumps({
			"id" : node_id,
			"treadable": treadables[node_id],
			"host": hosts[node_id]
		}, indent=2, sort_keys=True)


mw_config = []
for node_id in node_ids:
	mw_config.append(json.loads(get_json_for_single(node_id)))
FileWriter.write( \
	output_dir + output_file,
	json.dumps(mw_config, indent=2, sort_keys=True))
