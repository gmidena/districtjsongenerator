class TrackInfo:
    bus_ids = [ ]
    stops_per_districts_no = None
    bus_stop_ids = [ ]
    route_stops = { }
    first_stops = [ ]

    # BUSES

    @classmethod
    def set_buses_ids(cls, bus_ids):
        cls.bus_ids = bus_ids

    @classmethod
    def extract_bus(cls):
        head = cls.bus_ids[0]
        cls.bus_ids.pop(0)
        return head

    @classmethod
    def get_remaining_buses_no(cls):
        return len(cls.bus_ids)

    # STOPS PER DISTRICT

    @classmethod
    def set_stops_per_district_no(cls, new_stops_per_district_no):
        cls.stops_per_district_no = new_stops_per_district_no

    @classmethod
    def get_stops_per_district_no(cls):
        return cls.stops_per_district_no

    # BUS MAP

    @classmethod
    def set_first_stops(cls, new_first_stops):
        cls.first_stops = new_first_stops

    # TOTAL STOPS

    @classmethod
    def set_bus_stop_ids(cls, new_bus_stop_ids):
        cls.bus_stop_ids = new_bus_stop_ids

    @classmethod
    def extract_bus_stop_id(cls):
        i = 0
        #print cls.bus_stop_ids
        while True:
            id = cls.bus_stop_ids[i]
            if id not in cls.first_stops:
                cls.bus_stop_ids.remove(id)
                return id
            i = i + 1

    @classmethod
    def remove_bus_stop_id(cls, bus_stop_id):
        if bus_stop_id in cls.bus_stop_ids:
            cls.bus_stop_ids.remove(bus_stop_id)
            return True
        return False

    @classmethod
    def get_remaining_bus_stops_no(cls):
        return len(cls.bus_stop_ids)

    # ROUTE STOPS

    @classmethod
    def add_route_stop(cls, route_stop, bus_stop):
        cls.route_stops[bus_stop] = route_stop

    @classmethod
    def get_route_stop_from_bus_stop(cls, bus_stop):
        return cls.route_stops[bus_stop]
