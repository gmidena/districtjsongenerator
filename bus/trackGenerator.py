import copy

from bus.trackInfo import TrackInfo

from utils.id.agentIdGenerator import AgentIdGenerator
from utils.id.treadableIdGenerator import TreadableIdGenerator
from utils.randomizer import Randomizer

class TrackGenerator:
    def __init__(self):
        pass

    @staticmethod
    def generate(bus_requirements, districts_no):
        print("Generating bus tracks...")

        buses_no = bus_requirements["busesNumber"]
        stops_per_bus_no = bus_requirements["stopsPerBus"]
        stops_per_districts_no = bus_requirements["stopsPerDistrict"]
        total_stops_no = stops_per_districts_no * districts_no

        if total_stops_no < stops_per_bus_no:
            raise Exception("Too few bus stops w.r.t. configs")

        bus_stop_ids = []
        for i in range(total_stops_no):
            bus_stop_ids.append(TreadableIdGenerator.generate())
        bus_ids = []
        for i in range(buses_no):
            bus_ids.append(AgentIdGenerator.generate())

        TrackInfo.set_buses_ids(bus_ids)
        TrackInfo.set_stops_per_district_no(stops_per_districts_no)
        TrackInfo.set_bus_stop_ids(bus_stop_ids)

        unused_ids = copy.deepcopy(bus_stop_ids)
        bus_stops = {}

        for bus_id in bus_ids:
            bus_stops[bus_id] = []

        for bus_id in bus_ids:
            for i in range(stops_per_bus_no):
                if len(unused_ids) > 0:
                    unused_id = Randomizer.select_random(unused_ids)
                    bus_stops[bus_id].append(unused_id)
                    unused_ids.remove(unused_id)
                else:
                    bus_stop_id = Randomizer.select_random(bus_stop_ids)
                    while bus_stop_id in bus_stops[bus_id]:
                        bus_stop_id = Randomizer.select_random(bus_stop_ids)
                    bus_stops[bus_id].append(bus_stop_id)

        first_stops = [ ]
        for bus_id in bus_ids:
            first_stops.append(bus_stops[bus_id][0])
        TrackInfo.set_first_stops(first_stops)

        return bus_stops
