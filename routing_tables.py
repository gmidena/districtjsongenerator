# Generates routing tables for the middleware starting from district config
# files

# IMPORTANT: Call this *after* generating the input files for the city

import gc
import json
import sys

from utils.bfs import BFS
from utils.fileWriter import FileWriter
from utils.jsonFileLoader import JsonFileLoader

if len(sys.argv) <= 3:
    print("Usage: " + sys.argv[0]
            + " <city_name> <input_filename> <output_directory>")
    # hint: input_filename will be `resource/mw_config/routing`
    sys.exit()

city_name = sys.argv[1]
print("Routing tables for city " + city_name)

input_file = sys.argv[2]
print("Input file is " + input_file)

output_dir  = sys.argv[3]
print("You will find the output files in " + output_dir)

node_ids = []

global_info = JsonFileLoader.load(input_file)

neighbors = {}
# initialize neighbors
for node_id in node_ids:
    neighbors[node_id] = set()

for info in global_info:
    node_id = info["id"]
    node_ids.append(node_id)
    neighbors[node_id] = info["neighbors"]

for node_id in node_ids:
    while node_id in neighbors[node_id]:
        neighbors[node_id].remove(node_id)

bfs = BFS()

# If you want to test yourself with a bigger graph...
#new_nodes = ["A", "B", "C", "D", "E"]
#neighbors["A"] = set({"B", "C"})
#neighbors["B"] = set({"A", "D", "E"})
#neighbors["C"] = set({"A", "D", "E"})
#neighbors["D"] = set({"B", "C"})
#neighbors["E"] = set({"B", "C"})
#bfs.add_node("A", neighbors["A"])
#bfs.add_node("B", neighbors["B"])
#bfs.add_node("C", neighbors["C"])
#bfs.add_node("D", neighbors["D"])
#bfs.add_node("E", neighbors["E"])
#node_ids = new_nodes

for node_id in node_ids:
    bfs.add_node(node_id, neighbors[node_id])

routing_tables = {}
for node_id in node_ids:
    spanning_tree = bfs.spanning_tree(node_id)
    for st_key in spanning_tree.keys():
        spanning_tree[st_key] = spanning_tree[st_key][1]
    routing_tables[node_id] = spanning_tree

# key = destination
# decision = next node to which the message will be directed
def dump(routing_table):
    routes = []
    for routing_entry in routing_table.keys():
        routes.append(
            json.loads(
                json.dumps({
                    "key": routing_entry,
                    "decision": routing_table[routing_entry]
                })
            )
        )
    return routes

for node_id in node_ids:
    FileWriter.write( \
        output_dir + str(node_id) + ".conf",
        json.dumps({
            "id" : node_id,
            "neighbors": list(neighbors[node_id]),
            "routing_table": dump(routing_tables[node_id])
        }, indent=2, sort_keys=True))
