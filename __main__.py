import gc
import re
import sys

from os import listdir
from os.path import isfile, join

from active.entities.activeEntitiesGenerator import ActiveEntitiesGenerator
from active.config.activeInfo import ActiveInfo
from active.config.activeReader import ActiveReader

from bus.trackGenerator import TrackGenerator

from district.districtGenerator import DistrictGenerator

from infrastructures.facilities.facilityInfo import FacilityInfo

from scheduling.agendaGenerator import AgendaGenerator

from utils.filePathInputReceiver import FilePathInputReceiver
from utils.jsonFileLoader import JsonFileLoader
from utils.fileWriter import FileWriter

input_dir = "resource/input/"
output_dir = "resource/output/"
active_dir = "active/"
bus_dir = "bus_map/"
district_dir = "districts/"
agenda_dir = "agendas/"
# specific config files
active_config = "config_active.json"
bus_config = "config_bus.json"
# patterns
input_pattern = re.compile("^in-.*\.json$")

if len(sys.argv) >= 2:
    city = sys.argv[1]
    print("Generating city " + str(city))
if len(sys.argv) >= 3:
    input_dir = sys.argv[2]
    active_in=input_dir + active_dir
    bus_in=input_dir + bus_dir
    district_in=input_dir + district_dir
    print("Input dir is " + input_dir)
if len(sys.argv) >= 4:
    output_dir = sys.argv[3]
    active_out=output_dir + active_dir
    agenda_out=output_dir + agenda_dir
    district_out=output_dir + district_dir
    print("Output dir is " + output_dir)

active_files_in     = \
    [f for f in listdir(active_in) if isfile(join(active_in, f))]
district_files_in   = \
    [f for f in listdir(district_in) if isfile(join(district_in, f))]

correct_active_files = []
for active_file in active_files_in:
    if input_pattern.match(active_file):
        correct_active_files.append(active_file)
active_files_in = correct_active_files
active_files_in.sort()

correct_district_files = []
for district_file in district_files_in:
    if input_pattern.match(district_file):
        correct_district_files.append(district_file)
district_files_in = correct_district_files
district_files_in.sort()

# Check that there is a corresponding active file for each district file
if len(active_files_in) < len(district_files_in):
    raise Exception("There are less active files than district files")
if len(active_files_in) > len(district_files_in):
    raise Exception("There are less district files than active files")
for i in range(1,len(active_files_in)):
    if active_files_in[i] != district_files_in[i]:
        raise Exception("No corresponding file for" + \
                active_in + active_files_in[i])
for i in range(1,len(district_files_in)):
    if active_files_in[i] != district_files_in[i]:
        raise Exception("No corresponding file for" + \
                district_in + district_files_in[i])


no_of_districts = len(district_files_in)

# Read configuration data for active entities
file_path = FilePathInputReceiver.receive(active_in + active_config)
json = JsonFileLoader.load(file_path)
ActiveReader.read_general(json)

# Create intermediate file for bus tracks
file_path = FilePathInputReceiver.receive(bus_in + bus_config)
json = JsonFileLoader.load(file_path)
bus_map = TrackGenerator.generate(json, no_of_districts)


def get_suffix(x):
    return x[2:]

districts = {}
streets = {}
# Create config files for districts
for district_file in district_files_in:
    active_file = district_file
    file_suffix = get_suffix(district_file)
    file_path = FilePathInputReceiver.receive(active_in + active_file)
    json = JsonFileLoader.load(file_path)
    ActiveReader.read(json)
    # then generate district
    file_path = FilePathInputReceiver.receive(district_in + district_file)
    print(file_path)
    json = JsonFileLoader.load(file_path)
    district = DistrictGenerator.generate_streets(json, bus_map)
    # memorize streets for intersections
    for street in district.streets:
        streets[street.id] = street
    agenda_json = AgendaGenerator.generate()
    agenda_file = "agenda" + file_suffix
    FileWriter.write(agenda_out + agenda_file, agenda_json)
    ActiveInfo.save_and_reset_district_info(file_suffix)
    FacilityInfo.save_and_reset_district_info(file_suffix)
    # save district
    districts[district_file] = district

for district_file in district_files_in:
    file_suffix = get_suffix(district_file)
    district = districts[district_file]
    # reload district info
    file_path = FilePathInputReceiver.receive(district_in + district_file)
    json = JsonFileLoader.load(file_path)
    district = \
        DistrictGenerator.generate_intersections(json, district, streets)
    district_out_file = "out" + file_suffix
    FileWriter.write(district_out + district_out_file, district.get_json())
    ActiveInfo.augment(file_suffix)

for district_file in district_files_in:
    file_suffix = get_suffix(district_file)[:-5]
    district_id = file_suffix + ".json"
    active_entities = ActiveEntitiesGenerator.generate(district_id, bus_map)
    active_entities_json = active_entities.get_json()
    ae_out_file = "out" + file_suffix + ".json"
    FileWriter.write(active_out + ae_out_file, active_entities_json)
