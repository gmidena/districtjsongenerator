#!/bin/bash

# output filename
out_filename=${1:-}
# city name
name=${2:-}
# input directory
in_dir=${3:-}
# output directory
out_dir=${4:-}
# output file extension
out_ext=".json"

function help
{
   echo "Usage: $0 output_filename input_directory output_directory"
   echo "   example: $0 district resource/output/12/districts/ resource/collected/12/"
}

if [ -z ${out_filename} ] || [ -z ${in_dir} ] || [ -z ${out_dir} ];
then
   echo "[ERROR: missing argument]"
   help
   exit 1
fi

# overwrite file if it already exists
echo "{ \"nodes\" : [" > $out_dir$out_filename$out_ext

config_id=1
configs=($(ls $in_dir))

for config in "${configs[@]}"
do
   c_id=\"${config_id}\"
   if [ "$config_id" -eq "1" ]
   then
      echo "{ $c_id : " >> $out_dir$out_filename$out_ext
   else
      echo ",{ $c_id : " >> $out_dir$out_filename$out_ext
   fi
   cat $in_dir$config >> $out_dir$out_filename$out_ext
   echo "}" >> $out_dir$out_filename$out_ext
   config_id=$(( config_id+1 ))
done
echo "]}" >> $out_dir$out_filename$out_ext
