import json


class Traveller:

    def __init__(self, id, t_type, current_position):
        self.id = id
        self.t_type = t_type
        self.src = {}
        self.dst = {}
        self.residual_travel = []
        self.travel_state = "PLANNING"
        self.current_speed = 0
        self.max_speed = 0
        self.current_position = current_position
        self.passengers = []
        self.max_passengers = 0
        self.pvt_type = ""
        self.bus_stops = []
        self.route_stops = []
        self.is_waiting = False

    def get_json(self):
        return json.dumps({
            "id": self.id,
            "type": self.t_type,
            "src": json.loads(self.get_src_json()),
            "dst": json.loads(self.get_dst_json()),
            "residualTravel": self.residual_travel,
            "travelState": self.travel_state,
            "curSpeed": self.current_speed,
            "maxSpeed": self.max_speed,
            "curPosition": self.current_position,
            "passengers": self.passengers,
            "maxPassengers": self.max_passengers,
            "pvtType": self.pvt_type,
            "busStops": self.bus_stops,
            "routeStops": self.route_stops,
            "isWaiting": self.is_waiting
        })

    def set_src(self, src):
        self.src = src
    def set_dst(self, dst):
        self.dst = dst
    def set_max_speed(self, max_speed):
        self.max_speed = max_speed
    def set_max_passengers(self, max_passengers):
        self.max_passengers = max_passengers
    def set_pvt_type(self, pvt_type):
        self.pvt_type = pvt_type
    def set_bus_stops(self, bus_stops):
        self.bus_stops = bus_stops
    def set_route_stops(self, route_stops):
        self.route_stops = route_stops

    def get_id(self):
        return self.id
    def get_src(self):
        return self.src

    def get_src_json(self):
        return Traveller.get_slice_json(self.src)

    def get_dst_json(self):
        return Traveller.get_slice_json(self.dst)

    @staticmethod
    def get_slice_json(slice_obj):
        if "foot" not in slice_obj.keys():
            slice_obj["foot"] = []
        if "bike" not in slice_obj.keys():
            slice_obj["bike"] = []
        if "road" not in slice_obj.keys():
            slice_obj["road"] = []
        return json.dumps({
            "FOOT" : map(lambda x: x.get_id(), slice_obj["foot"]),
            "BIKE" : map(lambda x: x.get_id(), slice_obj["bike"]),
            "ROAD" : map(lambda x: x.get_id(), slice_obj["road"])
            })
