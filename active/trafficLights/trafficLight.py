import json


class TrafficLight:

    def __init__(self, id, period):
        self.id = id
        self.color = "RED"
        self.period = period

    def get_json(self):
        return json.dumps({
            "id": self.id,
            "color": self.color,
            "period": self.period
        })

    def get_id(self):
        return self.id
