import json


class ActiveEntities:

    def __init__(self, max_speed):
        self.max_speed = max_speed
        self.travellers = []
        self.traffic_lights = []

    def set_travellers(self, travellers):
        self.travellers = travellers

    def set_traffic_lights(self, traffic_lights):
        self.traffic_lights = traffic_lights

    def get_json(self):
        return json.dumps({
            "maxSpeed": self.max_speed,
            "travellers": self.get_travellers_json(),
            "trafficLights": self.get_traffic_lights_json()
        }, indent=2, sort_keys=True)

    def get_travellers_json(self):
        travellers_json = []
        for traveller in self.travellers:
            travellers_json.append(json.loads(traveller.get_json()))
        return travellers_json

    def get_traffic_lights_json(self):
        traffic_lights_json = []
        for traffic_light in self.traffic_lights:
            traffic_lights_json.append(json.loads(traffic_light.get_json()))
        return traffic_lights_json
