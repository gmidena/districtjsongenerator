from active.entities.activeEntities import ActiveEntities
from active.config.activeInfo import ActiveInfo

from bus.trackInfo import TrackInfo

from infrastructures.facilities.facilityInfo import FacilityInfo

from utils.randomizer import Randomizer


class ActiveEntitiesGenerator:
    def __init__(self):
        pass

    @staticmethod
    def generate(district_id, bus_map):
        facilities = FacilityInfo.get_facilities_for_district(district_id)

        max_speed = ActiveInfo.get_max_speed()

        pedestrians = ActiveInfo.get_pedestrians_for_district(district_id)

        bicycles = ActiveInfo.get_bicycles_for_district(district_id)

        buses = ActiveInfo.get_buses_for_district(district_id)
        for bus in buses:
            bus_id = bus.get_id()
            bus_stops = bus_map[bus_id]
            bus.set_bus_stops(bus_stops)
            route_stops = []
            for bus_stop in bus_stops:
                route_stops.append( \
                    TrackInfo.get_route_stop_from_bus_stop(bus_stop))
            bus.set_route_stops(route_stops)

        pvts = ActiveInfo.get_pvts_for_district(district_id)
        for pvt in pvts:
            pvt_type = ActiveEntitiesGenerator.get_type_for_pvt()
            pvt.set_pvt_type(pvt_type)
            pvt.set_max_speed(ActiveInfo.get_max_speed_for(pvt_type))
            pvt.set_max_passengers(ActiveInfo.get_max_passengers_for(pvt_type))

        traffic_lights = ActiveInfo.get_traffic_lights_for_district(district_id)

        # set src for pedestrians, bikes and pvts
        for pedestrian in pedestrians:
            pedestrian_slice = pedestrian.current_position
            pedestrian.set_src(facilities[pedestrian_slice])
        for bicycle in bicycles:
            bicycle_slice = bicycle.current_position
            bicycle.set_src(facilities[bicycle_slice])
        for pvt in pvts:
            pvt_slice = pvt.current_position
            pvt.set_src(facilities[pvt_slice])

        # set dst for pedestrians, bikes and pvts
        for pedestrian in pedestrians:
            pedestrian_facility = pedestrian.current_position
            dst = FacilityInfo.get_slice_from_different_facility( \
                district_id, pedestrian_facility)
            pedestrian.set_dst(dst)
        for bicycle in bicycles:
            bicycle_facility = bicycle.current_position
            dst = FacilityInfo.get_slice_from_different_facility( \
                district_id, bicycle_facility)
            bicycle.set_dst(dst)
        for pvt in pvts:
            pvt_facility = pvt.current_position
            dst = FacilityInfo.get_slice_from_different_facility( \
                district_id, pvt_facility)
            pvt.set_dst(dst)

        active_entities = ActiveEntities(max_speed)

        travellers = []
        travellers.extend(pedestrians)
        travellers.extend(bicycles)
        travellers.extend(buses)
        travellers.extend(pvts)
        active_entities.set_travellers(travellers)

        active_entities.set_traffic_lights(traffic_lights)

        return active_entities


    @staticmethod
    def get_type_for_pvt():
        prob_value = Randomizer.random(0, 100)
        if prob_value < 55:
            return ActiveInfo.PVT_TYPES[0]
        elif prob_value < 90:
            return ActiveInfo.PVT_TYPES[1]
        else:
            return ActiveInfo.PVT_TYPES[2]
