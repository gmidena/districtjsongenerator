from active.config.activeInfo import ActiveInfo

class ActiveReader:
    def __init__(self):
        pass

    @staticmethod
    def read_general(active_requirements):
        print("Reading active requirements...")

        speeds = active_requirements["speeds"]
        passengers = active_requirements["passengers"]
        tl_period = active_requirements["trafficLightPeriod"]

        traveller_types = ActiveInfo.TRAVELLER_TYPES
        vehicle_types   = ActiveInfo.VEHICLE_TYPES

        absolute_max_speed = speeds["absolute"]
        ActiveInfo.set_max_speed(absolute_max_speed)

        for traveller_type in traveller_types:
            max_speed = speeds[traveller_type]
            ActiveInfo.set_max_speed_for(max_speed, traveller_type)

        for vehicle_type in vehicle_types:
            max_passengers = passengers[vehicle_type]
            ActiveInfo.set_max_passengers_for(max_passengers, vehicle_type)

        ActiveInfo.set_traffic_light_period(tl_period)

    @staticmethod
    def read(active_requirements):
        pedestrians_no = active_requirements["pedestrians_no"]
        ActiveInfo.set_pedestrians_no(pedestrians_no)
        bicycles_no = active_requirements["bicycles_no"]
        ActiveInfo.set_bicycles_no(bicycles_no)
        buses_no = active_requirements["buses_no"]
        ActiveInfo.set_buses_no(buses_no)
        pvts_no = active_requirements["pvts_no"]
        ActiveInfo.set_pvts_no(pvts_no)
