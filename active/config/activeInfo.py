from active.travellers.traveller import Traveller
from active.trafficLights.trafficLight import TrafficLight

class ActiveInfo:
    max_speed = None
    speeds = {}
    passengers = {}
    total_buses_no = 0
    # per-district variables
    pedestrians_no = None
    bicycles_no = None
    buses_no = None
    pvts_no = None
    pedestrians_list = []
    bicycles_list = []
    buses_list = []
    pvts_list = []
    traffic_light_list = []
    # traffic-light variables
    traffic_light_period = None
    # district dumps
    districts = {}

    # Constants
    PEDESTRIAN = "PEDESTRIAN"
    BICYCLE    = "BICYCLE"
    BUS        = "BUS"
    CAR        = "CAR"
    MOTORCYCLE = "MOTORCYCLE"
    SIDECAR    = "SIDECAR"
    PVT        = "PVT"
    TRAVELLER_TYPES = [ PEDESTRIAN, BICYCLE, BUS, CAR, MOTORCYCLE, SIDECAR ]
    VEHICLE_TYPES   = [ BICYCLE, BUS, CAR, MOTORCYCLE, SIDECAR ]
    PVT_TYPES       = [ CAR, MOTORCYCLE, SIDECAR ]

    # MAX SPEED

    @classmethod
    def set_max_speed(cls, max_speed):
        cls.max_speed = max_speed

    @classmethod
    def get_max_speed(cls):
        if cls.max_speed == None:
            raise Exception("Max speed has not been set")
        return cls.max_speed

    # PER-TYPE SPEEDS

    @classmethod
    def set_max_speed_for(cls, max_speed, traveller_type):
        cls.speeds[traveller_type] = max_speed

    @classmethod
    def get_max_speed_for(cls, traveller_type):
        return cls.speeds[traveller_type]

    # PER-TYPE PASSENGERS

    @classmethod
    def set_max_passengers_for(cls, max_passengers, traveller_type):
        cls.passengers[traveller_type] = max_passengers

    @classmethod
    def get_max_passengers_for(cls, traveller_type):
        return cls.passengers[traveller_type]

    # BUSES

    @classmethod
    def increment_total_buses_no(cls):
        cls.total_buses_no = cls.total_buses_no + 1

    @classmethod
    def get_total_buses_no(cls):
        return cls.total_buses_no

    # PER-DISTRICT VARIABLES

    @classmethod
    def set_pedestrians_no(cls, pedestrians_no):
        cls.pedestrians_no = pedestrians_no

    @classmethod
    def get_pedestrians_no(cls):
        return cls.pedestrians_no

    @classmethod
    def set_bicycles_no(cls, bicycles_no):
        cls.bicycles_no = bicycles_no

    @classmethod
    def get_bicycles_no(cls):
        return cls.bicycles_no

    @classmethod
    def set_buses_no(cls, buses_no):
        cls.buses_no = buses_no

    @classmethod
    def get_buses_no(cls):
        return cls.buses_no

    @classmethod
    def set_pvts_no(cls, pvts_no):
        cls.pvts_no = pvts_no

    @classmethod
    def get_pvts_no(cls):
        return cls.pvts_no

    # TRAFFIC LIGHTS VARIABLES

    @classmethod
    def set_traffic_light_period(cls, traffic_light_period):
        cls.traffic_light_period = traffic_light_period

    @classmethod
    def get_traffic_light_period(cls):
        return cls.traffic_light_period

    # DISTRICT ACTIVE ENTITIES

    @classmethod
    def add_pedestrian(cls, pedestrian_id, current_position):
        pedestrian = \
            Traveller(pedestrian_id, ActiveInfo.PEDESTRIAN, current_position)
        pedestrian.set_max_speed(cls.speeds[ActiveInfo.PEDESTRIAN])
        cls.pedestrians_list.append(pedestrian)
    @classmethod
    def add_bicycle(cls, bicycle_id, current_position):
        bicycle = \
            Traveller(bicycle_id, ActiveInfo.BICYCLE, current_position)
        bicycle.set_max_speed(cls.speeds[ActiveInfo.BICYCLE])
        bicycle.set_max_passengers( \
            ActiveInfo.get_max_passengers_for(ActiveInfo.BICYCLE))
        cls.bicycles_list.append(bicycle)
    @classmethod
    def add_pvt(cls, pvt_id, current_position):
        pvt = \
            Traveller(pvt_id, ActiveInfo.PVT, current_position)
        # Max speed for PVT will be set when assigning the actual type
        # Max passengers for PVT will be set when assigning the actual type
        cls.pvts_list.append(pvt)
    @classmethod
    def add_bus(cls, bus_id, current_position):
        bus = \
            Traveller(bus_id, ActiveInfo.BUS, current_position)
        bus.set_max_speed(cls.speeds[ActiveInfo.BUS])
        bus.set_max_passengers( \
            ActiveInfo.get_max_passengers_for(ActiveInfo.BUS))
        cls.buses_list.append(bus)
    @classmethod
    def add_traffic_light(cls, traffic_light_id):
        traffic_light = \
            TrafficLight(traffic_light_id, cls.traffic_light_period)
        cls.traffic_light_list.append(traffic_light)

    @classmethod
    def get_pedestrians(cls):
        return cls.pedestrians_list
    @classmethod
    def get_bicycles(cls):
        return cls.bicycles_list
    @classmethod
    def get_buses(cls):
        return cls.buses_list
    @classmethod
    def get_pvts(cls):
        return cls.pvts_list
    @classmethod
    def get_traffic_lights(cls):
        return cls.traffic_light_list

    @classmethod
    def save_and_reset_district_info(cls, district_id):
        district = {}
        district["id"] = district_id
        district["pedestrians"] = cls.pedestrians_list
        district["bicycles"] = cls.bicycles_list
        district["buses"] = cls.buses_list
        district["pvts"] = cls.pvts_list
        district["traffic_lights"] = cls.traffic_light_list
        cls.districts[district_id] = district
        cls.pedestrians_list = []
        cls.bicycles_list = []
        cls.buses_list = []
        cls.pvts_list = []
        cls.traffic_light_list = []

    @classmethod
    def augment(cls, district_id):
        district = cls.districts[district_id]
        district["traffic_lights"] = cls.traffic_light_list
        cls.traffic_light_list = []

    @classmethod
    def get_pedestrians_for_district(cls, district_id):
        return cls.districts[district_id]["pedestrians"]
    @classmethod
    def get_bicycles_for_district(cls, district_id):
        return cls.districts[district_id]["bicycles"]
    @classmethod
    def get_buses_for_district(cls, district_id):
        return cls.districts[district_id]["buses"]
    @classmethod
    def get_pvts_for_district(cls, district_id):
        return cls.districts[district_id]["pvts"]
    @classmethod
    def get_traffic_lights_for_district(cls, district_id):
        return cls.districts[district_id]["traffic_lights"]
